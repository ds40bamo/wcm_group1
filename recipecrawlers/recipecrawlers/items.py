# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field

class BaseRecipesItem(Item):
    url = Field()
    date = Field()
    author = Field()
    title = Field()
    ingredients = Field()
    preptime = Field()
    steps = Field()
    amount = Field()
    image_urls = Field()
    images = Field()
    description = Field()


class SimplyRecipesItem(BaseRecipesItem):
    cooktime = Field()
    comments = Field()
    rating = Field()
    ratings = Field()
    categories = Field()
    main_categories = Field()
    cuisine = Field()
    author_url = Field()
    equipment = Field()
    totaltime = Field()


class FoodcomItem(BaseRecipesItem):
    cooktime = Field()
    comments = Field()
    rating = Field()
    ratings = Field()
    categories = Field()
    author_url = Field()
    main_category = Field()
    units = Field()
    nutrition = Field()


class EpicuriousItem(BaseRecipesItem):
    comments = Field()
    rating = Field()
    ratings = Field()
    categories = Field()
    author_url = Field()
    nutrition = Field()
    totaltime = Field()


class ChowhoundItem(BaseRecipesItem):
    totaltime = Field()
    rating = Field()
    ratings = Field()
    categories = Field()
    main_categories = Field()
    nutrition = Field()
    difficulty = Field()


class ThepioneerwomanItem(BaseRecipesItem):
    category = Field()
    cooktime = Field()
    difficulty = Field()
    author_url = Field()
    rating = Field()
    ratings = Field()


class SkinnytasteItem(BaseRecipesItem):
    totaltime = Field()
    comments = Field()
    rating = Field()
    ratings = Field()
    categories = Field()
    nutrition = Field()
    main_categories = Field()
    cuisine = Field()


class YummlyItem(BaseRecipesItem):
    totaltime = Field()
    # feed-#-content-tags-nutrition-#-'display-name'
    categories = Field()
    # feed-#-content-tags-technique-#-'display-name'
    technique = Field()
    # feed-#-content-tags-course-#-'display-name'
    main_category = Field()
    rating = Field()
    ratings = Field()
    author_url = Field()
    keywords = Field()
    yums = Field()
