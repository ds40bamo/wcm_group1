# -*- coding: utf-8 -*-

import re
from decimal import Decimal
from functools import reduce
from html.parser import unescape

import isodate
from scrapy.loader import ItemLoader
from scrapy.loader.processors import (Compose, Join, MapCompose, SelectJmes,
                                      TakeFirst)
from w3lib.html import remove_tags

clean_tags = re.compile('<.*?>')


class BaseItemLoader(ItemLoader):
    url_out = TakeFirst()
    title_out = Compose(
        TakeFirst(),
        unescape
    )
    description_out = Compose(
        TakeFirst(),
        unescape,
    )
    steps_out = MapCompose(unescape)
    ingredients_out = MapCompose(unescape)
    date_out = Compose(TakeFirst(), isodate.parse_datetime, lambda date: int(date.timestamp()))
    author_out = Compose(
        TakeFirst(),
        unescape
    )
    author_url_out = TakeFirst()
    ratings_out = Compose(TakeFirst(), int)
    comments_out = Compose(
        TakeFirst(),
        lambda x: x.replace(",", ""),
        int
    )
    amount_out = TakeFirst()
    preptime_out = Compose(TakeFirst(), lambda x: int(isodate.parse_duration(x).total_seconds()/60))
    cooktime_out = Compose(TakeFirst(), lambda x: int(isodate.parse_duration(x).total_seconds()/60))
    totaltime_out = Compose(TakeFirst(), lambda x: int(isodate.parse_duration(x).total_seconds()/60))
    nutrition_out = TakeFirst()
    difficulty_out = TakeFirst()
    preparation_out = Compose(
        MapCompose(lambda x: x.replace("\n", "").replace("\r", ""))
    )


class SimplyRecipesItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/100))
    preptime_out = Compose(
        TakeFirst(),
        lambda x: x.replace("D", "DT") if "D" in x else x,    # weird time format
        lambda x: x.replace(" days", "") if "D" in x else x,  # one recipe has weird formating
        lambda x: int(isodate.parse_duration(x).total_seconds()/60)
    )
    totaltime_out = Compose(
        TakeFirst(),
        lambda x: x.replace("D", "DT") if "D" in x else x,    # weird time format
        lambda x: x.replace(" days", "") if "D" in x else x,  # one recipe has weird formating
        lambda x: int(isodate.parse_duration(x).total_seconds()/60)
    )
    categories_out = Compose(
        TakeFirst(),
        unescape,
        lambda x: [s.strip() for s in x.split(',')]
    )


class FoodcomItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/5))
    comments_out = TakeFirst()
    main_category_out = TakeFirst()
    units_out = TakeFirst()


class EpicuriousItemLoader(BaseItemLoader):
    rating_out = Compose(
        TakeFirst(),
        lambda rating: list(map(Decimal, rating.split("/"))),
        lambda x: float(x[0]/x[1])
    )
    totaltime_out = TakeFirst()
    preptime_out = TakeFirst()
    steps_out = MapCompose(remove_tags, str.strip)
    categories_out = Compose(
        TakeFirst(),
        lambda x: [s.strip() for s in x.split(',')]
    )


class ChowhoundItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/5))
    description_out = Compose(
        TakeFirst(),
        unescape,
        unescape
    )
    steps_out = MapCompose(
        unescape,
        remove_tags
    )
    ingredients_out = MapCompose(remove_tags)


def parse_pioneer_time(time_str):
    if " Minutes" in time_str:
        return int(time_str.replace(" Minutes", ""))
    if " Hours" in time_str:
        return int(time_str.replace(" Hours", ""))
    else:
        return time_str


class ThepioneerwomanItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/5))
    category_out = TakeFirst()
    cooktime_out = Compose(
        TakeFirst(),
        parse_pioneer_time
    )
    preptime_out = Compose(
        TakeFirst(),
        parse_pioneer_time
    )
    ingredients_out = TakeFirst()


class SkinnytasteItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/5))
    cuisine_out = TakeFirst()
    categories_out = Compose(
        TakeFirst(),
        lambda x: [s.strip() for s in x.split(',')]
    )
    description_out = Compose(
        TakeFirst(),
        unescape,
    )
    steps_out = MapCompose(
        unescape,
        remove_tags
    )
    ingredients_out = MapCompose(
        unescape,
        remove_tags
    )


class YummlyItemLoader(BaseItemLoader):
    rating_out = Compose(TakeFirst(), lambda rating: float(Decimal(rating)/5))
    yums_out = TakeFirst()
    preptime_out = Compose(TakeFirst(), lambda x: int(x/60))
    totaltime_out = Compose(TakeFirst(), lambda x: int(x/60))
