# -*- coding: utf-8 -*-

from scrapy import Spider

from recipecrawlers.items import ThepioneerwomanItem
from recipecrawlers.itemloaders import ThepioneerwomanItemLoader

import logging


class ThepioneerwomanSpider(Spider):
    name = 'thepioneerwoman'
    allowed_domains = [
        'thepioneerwoman.com'
    ]
    start_urls = [
        'https://thepioneerwoman.com/cooking/',
        'https://thepioneerwoman.com/food-and-friends/'
    ]

    def parse(self, response):
        entry_list = response.css(".category-with-latest-filter-results a.post-card-permalink")
        for a in entry_list:
            yield response.follow(a, callback=self.parse_recipe)

        pagination = response.css(".pagination .page-numbers.current ~ a:not(.next)")
        if pagination:
            next_page = pagination[0]
            next_page_num = next_page.xpath("./text()").get()
            next_page_num = int(next_page_num)
            total_pages_num = pagination[-1].xpath("./text()").get()
            if next_page_num % 10 == 0:
                logging.info("current {}/{}".format(next_page_num, total_pages_num))
            yield response.follow(next_page, callback=self.parse)

    def parse_recipe(self, response):
        loader = ThepioneerwomanItemLoader(item=ThepioneerwomanItem(), response=response)
        meta_loader = loader.nested_css("meta")
        itemprop_loader = loader.nested_xpath("//*[@itemprop]")
        itemprop_loader.add_xpath("category", "./self::*[@itemprop='recipeCategory']/text()")
        itemprop_loader.add_xpath("rating", "./self::*[@itemprop='ratingValue']/text()")
        itemprop_loader.add_xpath("ratings", "./self::*[@itemprop='reviewCount']/text()")

        meta_loader.add_xpath("description", "./self::*[@property='og:description']/@content")
        meta_loader.add_xpath("date", "./self::*[@property='article:published_time']/@content")
        meta_loader.add_xpath("image_urls", "./self::*[@property='og:image' and contains(@content, 'thepioneerwoman')]/@content")

        loader.add_css("title", ".recipe-title ::text")
        meta_loader.add_xpath("title", "./self::*[@property='og:title']/@content")

        loader.add_value("url", response.url)

        author_loader = loader.nested_css(".byline a")
        author_loader.add_css("author", "::text")
        author_loader.add_css("author_url", "::attr(href)")

        recipe_loader = loader.nested_css(".recipe-summary:not(.wide)")

        time_loader = recipe_loader.nested_css(".recipe-summary-time dt")
        time_loader.add_xpath("preptime", "self::*[contains(text(), 'Prep')]/following-sibling::dd/text()")
        time_loader.add_xpath("difficulty", "self::*[contains(text(), 'Diff')]/following-sibling::dd/text()")
        time_loader.add_xpath("cooktime", "self::*[contains(text(), 'Cook')]/following-sibling::dd/text()")
        time_loader.add_xpath("amount", "self::*[contains(text(), 'Serv')]/following-sibling::dd/text()")

        panel_loader = recipe_loader.nested_css(".panel")
        panel_selector = panel_loader.selector

        if panel_selector:
            ingredients = list(map(str.strip, panel_selector[0].css(".list-ingredients span ::text").getall()))
            ingredients = dict(zip(ingredients[1::2], ingredients[0::2]))
            recipe_loader.add_value("ingredients", ingredients)

            steps = list(map(str.strip, panel_selector[1].css(".panel-body ::text").getall()))
            recipe_loader.add_value("steps", steps)

        return loader.load_item()
