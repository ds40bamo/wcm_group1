#!/bin/bash

trap exit SIGINT

spiders=(
    chowhound
    epicurious
    foodcom
    simplyrecipes
    skinnytaste
    thepioneerwoman
    yummly
)

for spider in "${spiders[@]}"; do
    scrapy crawl $spider
done
