# -*- coding: utf-8 -*-

from scrapy import Request
from scrapy.spiders import Spider

from recipecrawlers.items import FoodcomItem
from recipecrawlers.itemloaders import FoodcomItemLoader

from json import loads
import logging

def build_page_url(page_num):
    return 'https://api.food.com/services/mobile/fdc/search/sectionfront?pn={}&recordType=Recipe&sortBy=mostPopular'.format(page_num)

def build_recipe_url(recipe_id):
    return "https://api.food.com/services/mobile/v1/recipe/{}/info?scaleTo=1&mode=Metric".format(recipe_id)


class FoodcomSpider(Spider):
    name = 'foodcom'
    allowed_domains = [
        'www.food.com',
        'api.food.com',
        'img.sndimg.com',
    ]

    def start_requests(self):
        url = build_page_url(0)
        yield Request(url, callback=self.next_request, meta={"page_num": 1})

    def next_request(self, response):
        page_num = response.meta["page_num"]
        if page_num < 250 and response.status != 404:
            if page_num % 10 == 0:
                logging.info("current {}".format(page_num))
            results = response.xpath("//results")
            if len(results) != 0:
                relevant_results = results.xpath("self::*[has_photo=1]/recipe_id/text()")
                for recipe_id in relevant_results.getall():
                    yield Request(url=build_recipe_url(recipe_id), callback=self.parse_recipe)

                # recurse into next page
                url = build_page_url(page_num)
                yield Request(url, callback=self.next_request, meta={"page_num": page_num + 1})

    def parse_recipe(self, response):
        info = loads(response.text)
        loader = FoodcomItemLoader(item=FoodcomItem(), response=response)

        for ingredient_info in info["ingredients"]:
            quantity = ingredient_info.get("quantity", "?")

            ingredText = ingredient_info.get("ingredText")
            unit = ingredText.split()[0] if ingredText else None

            try:
                food = ingredient_info["hyperlinkFoodTextList"]["template1"]["text"]
            except:
                food = ingredient_info.get("ingredText") or ingredient_info["sectionText"]


            ingredient = {
                "quantity": quantity,
                "unit": unit,
                "food": food,
            }
            loader.add_value("ingredients", ingredient)

        loader.add_value("url", info["canonicalUrl"])
        loader.add_value("title", info["title"])
        loader.add_value("description", info["description"])
        loader.add_value("date", info["jsonLd"]["datePublished"])
        loader.add_value("author", info["author"]["displayName"])
        loader.add_value("author_url", info["author"]["memberUrl"])
        loader.add_value("rating", info["rating"])
        loader.add_value("ratings", info["reviewCount"])
        loader.add_value("comments", info["feedStats"]["reviewsCount"])
        loader.add_value("amount", (info["yield"]["quantity"], info["yield"]["text"]))
        loader.add_value("units", info["units"])
        loader.add_value("steps", [step["stepText"] for step in info["steps"]])
        loader.add_value("categories", [cat["slug"] for cat in info["categories"]])
        loader.add_value("image_urls", info["gallery"].get("defaultPhoto", {}).get("photoUrl"))
        loader.add_value("cooktime", info["jsonLd"]["cookTime"])
        loader.add_value("preptime", info["jsonLd"]["prepTime"])
        loader.add_value("main_category", info["jsonLd"].get("recipeCategory"))
        loader.add_value("nutrition", info["nutrition"])
        return loader.load_item()
