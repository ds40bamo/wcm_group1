# -*- coding: utf-8 -*-

from scrapy.spiders import Spider

from recipecrawlers.items import SkinnytasteItem
from recipecrawlers.itemloaders import SkinnytasteItemLoader

from json import loads
import logging
from itertools import chain


class SkinnytasteSpider(Spider):
    name = 'skinnytaste'
    allowed_domains = ['www.skinnytaste.com']
    start_urls = ['https://www.skinnytaste.com/recipes/']

    def parse(self, response):
        entry_list = response.css(".archive-post > a")
        entry_list = entry_list.xpath("./self::*[not(contains(@href, 'meal-plan')) and not(contains(@href, 'dinner-plan'))]")
        for a in entry_list:
            yield response.follow(a, callback=self.parse_recipe)

        pagination = response.css(".page-numbers.current ~ a:not(.next)")
        if pagination:
            next_page = pagination[0]
            next_page_num = int(next_page.xpath("./text()").get())
            total_pages_num = pagination[-1].xpath("./text()").get()
            if next_page_num % 10 == 0:
                logging.info("current {}/{}".format(next_page_num, total_pages_num))
            yield response.follow(next_page, callback=self.parse)

    def parse_recipe(self, response):
        loader = SkinnytasteItemLoader(item=SkinnytasteItem(), response=response)

        json_fields = response.xpath("//script[@type='application/ld+json']/text()")
        meta = loads(json_fields[0].get())

        loader.add_value("url", response.url)
        loader.add_value("description", meta.get("description"))
        loader.add_xpath("description", "//meta[@name='description']/@content")
        loader.add_xpath("description", "//meta[@property='og:description']/@content")
        loader.add_value("date", meta.get("datePublished"))
        loader.add_value("author", meta.get("author", {}).get("name"))
        loader.add_value("title", meta.get("name"))
        image = meta.get("image", [None])[0]
        if image:
            loader.add_value("image_urls", image)
        else:
            loader.add_xpath("image_urls", "//meta[@property='og:image']/@content")

        rating_info = meta.get("aggregateRating", {})
        loader.add_value("rating", rating_info.get("ratingValue"))
        loader.add_value("ratings", rating_info.get("ratingCount"))
        loader.add_css("comments", ".post-title .ccount ::text")
        loader.add_value("main_categories", meta.get("recipeCategory"))
        loader.add_value("cuisine", meta.get("recipeCuisine"))
        loader.add_value("categories", meta.get("keywords"))

        loader.add_value("ingredients", meta.get("recipeIngredient"))
        loader.add_value("totaltime", meta.get("totalTime"))
        loader.add_value("preptime", meta.get("prepTime"))

        def unpack_steps(steps):
            if not steps:
                return []
            if type(steps) == dict:
                if "itemListElement" in steps:
                    return unpack_steps(steps["itemListElement"])
                else:
                    return [steps["text"]]
            if type(steps) == str:
                return [steps]
            else:
                return list(chain(*[unpack_steps(step) for step in steps]))

        instructions = meta.get("recipeInstructions")
        if instructions:
            loader.add_value("steps", unpack_steps(instructions))

        loader.add_value("amount", meta.get("recipeYield"))

        nutrition = meta.get("nutrition")
        if nutrition:
            del(nutrition["@type"])
        loader.add_value("nutrition", nutrition)

        return loader.load_item()
