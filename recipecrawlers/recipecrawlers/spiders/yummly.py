import logging
import json

from scrapy.spiders import Spider

from recipecrawlers.itemloaders import YummlyItemLoader
from recipecrawlers.items import YummlyItem


class YummlySpider(Spider):
    name = 'yummly'
    allowed_domains = ['mapi.yummly.com']
    start_urls = ['https://mapi.yummly.com/mapi/v17/content/search?solr.seo_boost=new&start={}&maxResult={}&fetchUserCollections=false&allowedContent=single_recipe&allowedContent=suggested_search&allowedContent=related_search&allowedContent=article&allowedContent=video&allowedContent=generic_cta&guided-search=true&solr.view_type=search_internal'.format(i, i+100) for i in range(0, 2000, 100)]

    def parse(self, response):
        response_json = json.loads(response.text)
        packet_ind = response.url.find('&start=') + 7
        packet = response.url[packet_ind:packet_ind+4]
        if packet[-1] == '&':
            packet = packet[:-1]
        elif packet[-3:] == '&ma':
            packet = packet[0]
        try:
            packet = int(packet)//100
        except ValueError:
            logging.info(packet)
        entry_list = response_json['feed']
        logging.info('Processing packet {}'.format(packet))
        for a in entry_list:
            yield self.parse_recipe(a)

    def parse_recipe(self, recipe):
        loader = YummlyItemLoader(item=YummlyItem())

        loader.add_value('url', recipe['seo']['web']['link-tags'][0]['href'])
        loader.add_value('author', recipe['display']['profiles'][0]['displayName'])
        loader.add_value('title', recipe['display']['displayName'])

        # cropping ingredients subdictionaries
        ingr_list = recipe['content']['ingredientLines']
        ingredients = \
            [
                {
                    'category' : ingredient['category'],
                    'quantity' : ingredient['quantity'],
                    'unit' : ingredient['unit'],
                    'ingredient' : ingredient['ingredient'],
                    'remainder' : ingredient['remainder'],
                    'whole_line' : ingredient['wholeLine']
                } for ingredient in ingr_list
            ]
        loader.add_value('ingredients', ingredients)

        # maybe replace value wth totaltime
        loader.add_value('preptime', 0)
        loader.add_value('steps', recipe['content']['preparationSteps'])
        loader.add_value('amount', recipe['content']['details']['numberOfServings'])
        loader.add_value('image_urls', recipe['display']['images'])
        description = recipe['content']['description']['text'] if \
            recipe['content']['description'] else ''
        loader.add_value('description', description)

        # YummlyItem specific fields
        loader.add_value('totaltime', recipe['content']['details']['totalTimeInSeconds'])
        nutrition = [t['display-name'] for t in recipe['content']['tags']['nutrition']] if \
            'nutrition' in recipe['content']['tags'] else []
        loader.add_value('categories', nutrition)
        technique = [t['display-name'] for t in recipe['content']['tags']['technique']] if \
            'technique' in recipe['content']['tags'] else []
        loader.add_value('technique', technique)
        main_category = [t['display-name'] for t in recipe['content']['tags']['course']] if \
            'course' in recipe['content']['tags'] else []
        loader.add_value('main_category', main_category)
        difficulty = [t['display-name'] for t in recipe['content']['tags']['difficulty']] if \
            'difficulty' in recipe['content']['tags'] else []
        loader.add_value('main_category', main_category)

        loader.add_value('rating', recipe['content']['reviews']['averageRating'])
        loader.add_value('ratings', recipe['content']['reviews']['totalReviewCount'])
        loader.add_value('author_url', recipe['display']['profiles'][0]['profileUrl'])
        loader.add_value('keywords', recipe['content']['details']['keywords'])
        loader.add_value('yums', recipe['content']['yums']['count'])

        return loader.load_item()
