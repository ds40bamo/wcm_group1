# -*- coding: utf-8 -*-

import logging
from json import loads
from itertools import chain

from scrapy.spiders import Spider

from recipecrawlers.itemloaders import SimplyRecipesItemLoader
from recipecrawlers.items import SimplyRecipesItem


class SimplyRecipesSpider(Spider):
    name = 'simplyrecipes'
    allowed_domains = ['www.simplyrecipes.com']
    start_urls = ['https://www.simplyrecipes.com/?s']

    def parse(self, response):
        entry_list = response.css(".entry-list").xpath(".//a[contains(@href, '/recipes/')]")
        for a in entry_list:
            yield response.follow(a, callback=self.parse_recipe)

        pagination = response.css(".page-numbers.current ~ a")
        if pagination:
            next_page = pagination[0]
            next_page_num = next_page.xpath("./text()").get()
            next_page_num = int(next_page_num)
            total_pages_num = pagination[-1].xpath("./text()").get()
            if next_page_num % 10 == 0:
                logging.info("current {}/{}".format(next_page_num, total_pages_num))
            yield response.follow(next_page, callback=self.parse)

    def parse_recipe(self, response):
        loader = SimplyRecipesItemLoader(item=SimplyRecipesItem(), response=response)

        loader.add_value("url", response.url)

        meta_loader = loader.nested_css("meta")
        meta_loader.add_xpath("date", "./self::*[@name='shareaholic:article_published_time']/@content")

        meta = loads(response.xpath("//script[@type='application/ld+json']/text()").get())
        loader.add_value("title", meta.get("name"))
        loader.add_value("description", meta.get("description"))
        loader.add_value("author", meta.get("author", {}).get("name"))
        loader.add_value("image_urls", meta.get("image"))
        loader.add_value("cooktime", meta.get("cookTime"))
        loader.add_value("preptime", meta.get("prepTime"))
        loader.add_value("totaltime", meta.get("totalTime"))
        loader.add_value("ingredients", meta.get("recipeIngredient"))
        loader.add_value("categories", meta.get("keywords"))
        loader.add_value("main_categories", meta.get("recipeCategory"))
        loader.add_value("cuisine", meta.get("recipeCuisine"))
        loader.add_value("amount", meta.get("recipeYield"))

        def unpack_steps(steps):
            if not steps:
                return []
            if type(steps) == dict:
                if "itemListElement" in steps:
                    return unpack_steps(steps["itemListElement"])
                else:
                    return [steps["text"]]
            if type(steps) == str:
                return [steps]
            else:
                return list(chain(*[unpack_steps(step) for step in steps]))

        instructions = meta.get("recipeInstructions")
        if instructions:
            loader.add_value("steps", unpack_steps(instructions))

        author_loader = loader.nested_css(".author")
        author_loader.add_css("author", "span::text")
        author_loader.add_css("author_url", "a::attr(href)")

        loader.add_css("rating", ".sr-recipe-rating .rating-value::attr(style)", re="width:(.*)px")
        loader.add_css("ratings", ".total-count.ratings::text")
        loader.add_value("ratings", "0")
        loader.add_css("comments", ".total-count.comments::text")
        loader.add_value("comments", "0")
        loader.add_css("equipment", "#sr-recipe-callout .ingredient_section_header + * span ::text")

        return loader.load_item()
