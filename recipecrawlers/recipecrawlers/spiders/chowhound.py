# -*- coding: utf-8 -*-

from scrapy.spiders import Spider

from recipecrawlers.items import ChowhoundItem
from recipecrawlers.itemloaders import ChowhoundItemLoader

from json import loads
from itertools import chain
import logging


class ChowhoundSpider(Spider):
    name = 'chowhound'
    allowed_domains = ['www.chowhound.com']
    start_urls = ['https://www.chowhound.com/recipes']

    def parse(self, response):
        entry_list = response.css(".fr_bxfoot > a")
        for a in entry_list:
            yield response.follow(a, callback=self.parse_recipe)

        pagination = response.css(".pagination span.page a")
        next_page = pagination.xpath("./self::*[@rel='next']")
        if next_page:
            next_page = next_page[0]
            next_page_num = int(next_page.xpath("./text()").get())
            total_pages_num = pagination[-1].xpath("./text()").get()
            if next_page_num % 10 == 0:
                logging.info("current {}/{}".format(next_page_num, total_pages_num))
            yield response.follow(next_page, callback=self.parse)

    def parse_recipe(self, response):
        loader = ChowhoundItemLoader(item=ChowhoundItem(), response=response)

        meta_str = response.xpath("//script[@type='application/ld+json']/text()").get()
        # replace linefeed because some websites have weird format
        meta_str = meta_str.replace("\n", " ").replace("\r", " ").replace("\t", "")
        meta = loads(meta_str)

        loader.add_value("url", response.url)
        loader.add_value("description", meta.get("description"))
        loader.add_value("date", meta.get("datePublished"))
        loader.add_value("author", meta.get("author", {}).get("name"))
        loader.add_value("title", meta.get("name"))
        loader.add_value("image_urls", meta.get("image", {}).get("url"))
        loader.add_css("difficulty", ".frr_difficulty ::text")

        rating_info = meta.get("aggregateRating", {})
        loader.add_value("rating", rating_info.get("ratingValue"))
        loader.add_value("ratings", rating_info.get("ratingCount"))
        loader.add_value("ratings", "0")
        loader.add_value("main_categories", meta.get("recipeCategory"))
        loader.add_value("categories", meta.get("keywords"))

        loader.add_value("ingredients", meta.get("recipeIngredient"))
        loader.add_value("totaltime", meta.get("totalTime"))
        loader.add_value("preptime", meta.get("prepTime"))

        def unpack_steps(steps):
            if not steps:
                return []
            if type(steps) == dict:
                if "itemListElement" in steps:
                    return unpack_steps(steps["itemListElement"])
                else:
                    return [steps["text"]]
            if type(steps) == str:
                return [steps]
            else:
                return list(chain(*[unpack_steps(step) for step in steps]))

        instructions = meta.get("recipeInstructions")
        if instructions:
            loader.add_value("steps", unpack_steps(instructions))
        loader.add_value("amount", meta.get("recipeYield"))

        nutrition = meta.get("nutrition")
        if nutrition:
            del(nutrition["@type"])
        loader.add_value("nutrition", nutrition)

        return loader.load_item()
