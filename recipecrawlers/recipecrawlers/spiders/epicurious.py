# -*- coding: utf-8 -*-

from scrapy import Spider

from recipecrawlers.items import EpicuriousItem
from recipecrawlers.itemloaders import EpicuriousItemLoader

import logging


class EpicuriousSpider(Spider):
    name = 'epicurious'
    allowed_domains = ['www.epicurious.com']
    start_urls = ['http://www.epicurious.com/search?page=1&xhr=true']

    def parse(self, response):
        entry_list = response.css(".recipe-content-card .view-complete-item:first-child")
        for a in entry_list:
            yield response.follow(a, callback=self.parse_recipe)

        pagination = response.css(".common-pagination .the-current-page ~ :not(.the-next-page)")
        if pagination:
            next_page = pagination[0]
            next_page_num = int(next_page.xpath("./text()").get())
            total_pages_num = pagination[-1].xpath("./text()").get()
            url = next_page.xpath("@href").get() + "&xhr=true"
            if next_page_num % 10 == 0:
                logging.info("current {}/{}".format(next_page_num, total_pages_num))
            yield response.follow(next_page, callback=self.parse)
            yield response.follow(url, callback=self.parse)

    def parse_recipe(self, response):
        loader = EpicuriousItemLoader(item=EpicuriousItem(), response=response)
        loader.add_css("image_urls", "picture > source:first-child::attr(srcset)")
        meta_loader = loader.nested_css("meta")
        meta_loader.add_xpath("description", "./self::*[contains(@name, 'description')]/@content")
        meta_loader.add_xpath("date", "./self::*[contains(@itemprop, 'datePublished') or contains(@name, 'parsely-pub-date')]/@content")
        meta_loader.add_xpath("author", "./self::*[@itemprop='author']/@content")
        meta_loader.add_xpath("categories", "./self::*[@name='keywords']/@content")
        meta_loader.add_xpath("title", "./self::*[@property='og:title']/@content")

        loader.add_value("url", response.url)
        author_loader = loader.nested_css("a.contributor")
        author_loader.add_css("author", "::attr(title)")
        author_url = response.urljoin(author_loader.selector.css("::attr(href)").get())
        author_loader.add_value("author_url", author_url)

        loader.add_css("rating", ".rating ::text")
        loader.add_css("ratings", ".reviews-count ::text")
        loader.add_value("ratings", "0")
        loader.add_css("comments", ".reviews-count ::text")
        loader.add_value("comments", "0")

        recipe_loader = loader.nested_css(".recipe-and-additional-content")
        recipe_loader.add_css("ingredients", ".ingredient ::text")
        recipe_loader.add_css("totaltime", "dd.total-time ::text")
        recipe_loader.add_css("preptime", "dd.active-time ::text")
        recipe_loader.add_css("steps", ".preparation-step")
        recipe_loader.add_css("amount", "dd.yield ::text")

        nutrition = recipe_loader.selector.css(".nutrition ul span ::text").getall()
        nutrition = dict(zip(nutrition[0::2], nutrition[1::2]))
        recipe_loader.add_value("nutrition", nutrition)

        return loader.load_item()
