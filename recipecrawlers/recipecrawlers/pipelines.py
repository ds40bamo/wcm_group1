# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


from scrapy.exceptions import DropItem


class BaseCleanPipeline():
    def process_item(self, item, spider):
        if not item.get("ingredients"):
            raise DropItem("missing recipe for {}".format(item.get("url")))
#         if not item.get("steps"):
#             raise DropItem("missing instructions for {}".format(item.get("url")))
#         if not item.get("description"):
#             raise DropItem("missing description for {}".format(item.get("url")))

        return item


class NoImageDropPipeline():
    def process_item(self, item, spider):
        if not item.get("images"):
            raise DropItem("missing image for {}".format(item.get("url")))

        return item
