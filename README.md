# WCM Praktikum - Group 1

## Objective
Creation of a web application for supported **ingredients** and **recipe** selection

## Data sources
#### Recipe sites
- [chowhound](https://www.chowhound.com/)
- [epicurious](https://www.epicurious.com/)
- [foodcom](https://www.food.com/)
- [simplyrecipes](https://www.simplyrecipes.com/)
- [skinnytaste](https://www.skinnytaste.com/)
- [thepioneerwoman](https://thepioneerwoman.com/)
- [yummly](https://www.yummly.com/)

#### Nutritional values
[USDA](https://fdc.nal.usda.gov/download-datasets.html): SR Legacy (the data is already contained in the repository)

## Important notes
- only tested on linux
- only tested with python 3.8
- only tested with firefox

## Tools
#### Required
- [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) (enable with: `systemctl start docker.service`)
- [docker-compose](https://docs.docker.com/compose/install/) (ubuntu: `sudo apt install docker-compose`)
- python3 (developed with 3.8)

#### Optional
- pipenv (`pip install pipenv`)
    - run `pipenv install` in the folders with a Pipfile bevor running `pipenv run <command>`
    - use `pipenv run python3 <file>` instead of `python3 <file>`
    - when not using pipenv, you need to manually install the dependencies from the **"Pipfile"** files

## Required Steps
1. Crawl the data
2. Parse the recipes
3. Run the application

## Setup
#### Crawl the data
1. open terminal
2. navigate into `recipecrawlers/recipecrawlers/spiders` from project root (run `pipenv install`)
3. crawl all with `bash run_all.sh` (`pipenv run bash run_all.sh`) (takes ~2 hours) or alternatively crawl only one site with
   `scrapy crawl <spider>` (`pipenv run scrapy crawl <spider>`), where `<spider>` is one of
   `chowhound, epicurious, foodcom, simplyrecipes, skinnytaste, thepioneerwoman, yummly`
    - `CTRL-C` once: stop the crawl, crawled data to this point will not be lost
    - `CTRL-C` twice: force stop crawler, sites in queue will not be crawled and output file is not valid json, because of missing end bracket

The crawled data will be stored in `~/.cache/recipecrawlers` where `~` is your home folder.

**Note:** You can change the directory where the data gets stored by editing the `RECIPE_DIR`
variable in the `.env` file which lies in the project root. Doing so can lead to undefined
(and untested) behavior.

#### Parse the recipes
1. open terminal
2. navigate into `logic` from project root (run `pipenv install`)
3. parse the data with `python3 add_nutrient.py` (`pipenv run python3 add_nutrient.py`)

#### Run the application
1. open terminal
2. start docker daemon if not running with `systemctl start docker.service`
3. navigate to project root and run `docker-compose up` (make sure no service is running on localhost port 80)
4. after a short while (&lt; 1 min, more if docker images have to be pulled) you can access the service via typing `localhost` into the addressbar of your browser

#### tldr
assumes the repository is in home folder and named `wcm_group1`
1. `cd ~/wcm_group1/recipecrawlers/recipecrawlers/spiders && pipenv install && pipenv run bash run_all.sh`
2. `cd ~/wcm_group1/logic && pipenv install && pipenv run python3 add_nutrient.py`
3. `systemctl start docker.service && cd ~/wcm_group1 && docker-compose up`
4. open browser, type `localhost` into addressbar

## Questions
#### What do the **barcharts** display?
It shows how much of the following nutritional values is in **100 gram** of the respective recipe:
- energy (in proportion to the daily requirements)
- protein (in gram)
- carbohydrate (in gram)
- fat (in gram)
- sugar (in gram)
- calcium (in proportion to the daily requirements)
- iron (in proportion to the daily requirements)
- zinc (in proportion to the daily requirements)
- magnesium (in proportion to the daily requirements

#### What does the **"show similar"** button do?
It sorts the distance between the chosen recipe and all other recipes and shows the **10 most similar**.  
The distance measure is the **cosine distance** between the vectors holding the **nutritional information** (see "What do the barcharts display?")  
The distances are precomputed and stored.  
To update the index you need to remove the `vector_data` volume (`docker volume rm wcm_group1_vector_data`) and restart the similarity container (`docker-compose restart similarity`).

#### Why is the table displaying the ingredients not consistent?
If the ingredient was successfully parsed, quantity, unit and ingredient is column wise displayed. Otherwise the original ingredient string is displayed.  
**Note:** successfully parsed means that the parser gives a result and doesn't throw an exception. The results the parser gives are not really good, because it is hard to recognize the ingredients in the raw string and link them to the reference database.

#### Why are some images not displayed or the recipes not updated?
The recipe thumbnails are mounted from the `~/.cache/recipecrawlers/images/full` path and the recipes are mounted from the `~/.cache/recipecrawlers/sites` path.  
The recipes are additionally stored in the elasticsearch index. The index is only created and updated if it does not already exist.   
To update the index you need to remove the `elasticsearch_data` volume (`docker volume rm wcm_group1_elasticsearch_data`) and restart the search container (`docker-compose restart search`).

## Preview
#### Index
![index preview](images/index.png)
#### Search
![search preview](images/search.png)
#### Recipe
![recipe preview](images/recipe.png)
