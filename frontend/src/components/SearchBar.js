import React, { useRef } from "react";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Button from "react-bootstrap/Button";
import Navbar from "react-bootstrap/Navbar";
import { FaSearch } from "react-icons/fa";
import { GiSpoon } from "react-icons/gi";
import history from "../common/history";

const SearchBar = props => {
  const inputRef = useRef();
  const query = props.query !== undefined ? props.query : ""

  const submit = () => {
    const inputValue = inputRef.current.value;
    if (inputValue !== "") {
      history.push("/search?query=" + encodeURIComponent(inputRef.current.value));
    }
  };

  const onKeyDown = e => {
    if (e.keyCode === 13) {
      submit();
    }
  };

  return (
    <Navbar bg="white" sticky="top">
      <Navbar.Brand href="/">
        <div className="d-flex">
          <GiSpoon
            className="mr-3 align-self-center"
            style={{ color: "orange" }}
          />
          RECIPE SEARCH
        </div>
      </Navbar.Brand>
      <InputGroup className="shadow">
        <FormControl
          ref={inputRef}
          type="text"
          defaultValue={query}
          onKeyDown={onKeyDown}
        />
        <InputGroup.Append>
          <Button onClick={submit}>
            <FaSearch />
          </Button>
        </InputGroup.Append>
      </InputGroup>
    </Navbar>
  );
};

export default SearchBar;
