import React from "react";
import Image from "react-bootstrap/Image";

const RecipeThumbnail = props => {
  return (
    <Image
      className={props.className}
      src={props.src}
      height="200px"
      width="300px"
      style={{ "objectFit": "cover" }}
    />
  );
};

export default RecipeThumbnail;
