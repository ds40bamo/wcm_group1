import React from "react";
import Badge from "react-bootstrap/Badge";
import Table from "react-bootstrap/Table";

function hashCode(str) {
  // java String#hashCode
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

function intToRGB(i) {
  var c = (i & 0x00ffffff).toString(16).toUpperCase();

  return "00000".substring(0, 6 - c.length) + c;
}

function hexToRgb(hex) {
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;

    return [r, g, b]
}

function foreground(background) {
  const [r, g, b] = hexToRgb(background)
  return ((r*0.299 + g*0.587 + b*0.114) > 150) ? "000000" : "ffffff"
}


const AuthorField = props => {
  const name = props.authorName;
  const url = props.authorURL;
  if (name !== undefined) {
    if (url !== undefined) {
      return (
        <Badge className={props.className} as="a" variant="primary" href={url}>
          by {name}
        </Badge>
      );
    } else {
      return (
        <Badge className={props.className} variant="secondary">
          by {name}
        </Badge>
      );
    }
  } else {
    return (
      <Badge className={props.className} variant="danger">
        unknown author
      </Badge>
    );
  }
};

export { AuthorField };

const RatingField = props => {
  const ratings = props.ratings;
  const rating = props.rating;
  if (ratings === undefined || ratings <= 0) {
    return (
      <Badge className={props.className} variant="secondary">
        no ratings
      </Badge>
    );
  } else {
    return (
      <Badge className={props.className} variant="warning">
        {(rating * 100).toFixed(0)}% with {ratings} ratings
      </Badge>
    );
  }
};

export { RatingField };

const CommentsField = props => {
  const comments = props.comments;
  if (comments === undefined || comments <= 0) {
    return (
      <Badge className={props.className} variant="secondary">
        no comments
      </Badge>
    );
  } else {
    return (
      <Badge className={props.className} variant="warning">
        {comments} comments
      </Badge>
    );
  }
};

export { CommentsField };

const CategoriesField = props => {
  const categories = props.categories;
  if (categories !== undefined) {
    return (
      <div className={props.className}>
        {categories.map((categorie, i) => (
          <Badge key={i} className="my-1 mr-2" variant="secondary">
            {categorie}
          </Badge>
        ))}
      </div>
    );
  } else {
    return <></>;
  }
};

export { CategoriesField };

const MainCategoriesField = props => {
  const mainCategories = props.mainCategories;
  if (mainCategories !== undefined) {
    return (
      <div className={props.className}>
        {mainCategories.map((categorie, i) => (
          <Badge key={i} className="mr-2" variant="success">
            {categorie}
          </Badge>
        ))}
      </div>
    );
  } else {
    return (
      <Badge className={props.className} variant="secondary">
        no main categorie
      </Badge>
    );
  }
};

export { MainCategoriesField };

const DescriptionField = props => {
  const cutlength = 600;
  let description = props.description;

  if (description !== undefined) {
    description = props.description.slice(0, cutlength);
    if (description.length === cutlength) {
      description = description + " ...";
    }
    return <>{description}</>;
  } else {
    return <>no description</>;
  }
};

export { DescriptionField };

const CuisineField = props => {
  const cuisine = props.cuisine;
  if (cuisine !== undefined) {
    return (
      <Badge className={props.className} variant="danger">
        {cuisine}
      </Badge>
    );
  } else {
    return <></>;
  }
};

export { CuisineField };

const DateField = props => {
  const timestamp = props.timestamp;
  if (timestamp !== undefined) {
    const date = new Date(timestamp * 1000);
    return (
      <Badge className={props.className} variant="light">
        {date.toLocaleDateString()}
      </Badge>
    );
  } else {
    return (
      <Badge className={props.className} variant="secondary">
        unknown publish date
      </Badge>
    );
  }
};

export { DateField };

const PublisherField = props => {
  const publisher = props.publisher;
  const url = props.url;
  let bgcolor = intToRGB(hashCode(props.publisher))
  const fgcolor = foreground(bgcolor)
  bgcolor = "#" + bgcolor
  if (publisher !== undefined) {
    if (url !== undefined) {
      return (
        <>
        <Badge as="a" style={{backgroundColor: bgcolor, color: fgcolor}} className={props.className} href={url}>
          {publisher}
        </Badge>
        </>
      );
    } else {
      return (
        <Badge className={props.className} variant="light">
          {publisher}
        </Badge>
      );
    }
  } else {
    return (
      <Badge className={props.className} variant="secondary">
        no publisher
      </Badge>
    );
  }
};

export { PublisherField };

const IngredientField = props => {
  const ingredients = props.ingredients;
  return (
    <Table>
      <thead>
        <tr>
          <th>qantity</th>
          <th>unit</th>
          <th>ingredient</th>
        </tr>
      </thead>
      <tbody>
        {ingredients.map(({ unit, quantity, ingredient, whole_line }, i) => {
          if (typeof whole_line !== "string") {
            whole_line = Object.values(whole_line).join(" ");
          }
          if (ingredient === "") {
            return (
              <tr key={i}>
                <td colSpan="3">{whole_line}</td>
              </tr>
            );
          } else {
            return (
              <tr key={i}>
                <td>{quantity}</td>
                <td>{unit}</td>
                <td>{ingredient}</td>
              </tr>
            );
          }
        })}
      </tbody>
    </Table>
  );
};

export { IngredientField };

const InstructionsField = props => {
  const instructions = props.instructions;
  if (instructions !== undefined) {
    return (
      <ol className="pt-2">
        {instructions.map((instruction, i) => (
          <li key={i} className="p-2">
            {instruction}
          </li>
        ))}{" "}
        <br />
      </ol>
    );
  } else {
    return <h3 className="border p-3 d-inline-flex">no instructions</h3>;
  }
};

export { InstructionsField };
