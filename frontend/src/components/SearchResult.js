import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import RecipeThumbnail from "./RecipeThumbnail";
import NutrientChart from "./NutrientChart";
import Button from "react-bootstrap/Button";
import Image from "react-bootstrap/Image";
import { Link } from "react-router-dom";

import {
  AuthorField,
  RatingField,
  CommentsField,
  DateField,
  PublisherField,
  DescriptionField
} from "./InfoFields";

function hashCode(str) {
  // java String#hashCode
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.charCodeAt(i) + ((hash << 5) - hash);
  }
  return hash;
}

function intToRGB(i) {
  var c = (i & 0x00ffffff).toString(16).toUpperCase();

  return "00000".substring(0, 6 - c.length) + c;
}

const SearchResult = props => {
  const [mouseOver, setMouseOver] = useState(false);
  const onMouseOver = () => {
    setMouseOver(true);
  };
  const onMouseOut = () => {
    setMouseOver(false);
  };
  const bgcolor = "#" + intToRGB(hashCode(props.data.publisher))
  return (
    <Card
      style={{
        borderColor: bgcolor,
        borderWidth: "3px"
      }}
      className={"mb-3 " + (mouseOver ? "" : "shadow")}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
    >
      <Card.Header>
        <div className="d-flex flex-column flex-md-row justify-content-between align-content-center">
          <Link
            to={{
              pathname: "/recipe/" + encodeURIComponent(props.id),
              state: { query: props.query }
            }}
            style={{ color: "inherit", textDecoration: "none" }}
          >
            <h4
              className="align-self-md-center"
              style={{ fontFamily: "monospace" }}
            >
              {props.data.title}
            </h4>
          </Link>
          <div className="d-flex">
            <Button className="m-1" variant="primary" href={props.data.url}>
              go to source
            </Button>
            <Button
              className="m-1"
              as={Link}
              to={{
                pathname: "/similarity/" + encodeURIComponent(props.id),
                state: { query: props.query }
              }}
              style={{ color: "white", textDecoration: "none" }}
              variant="secondary"
            >
              show similar
            </Button>
          </div>
        </div>
        <PublisherField
          className="mr-3 my-1"
          publisher={props.data.publisher}
          url={props.data.url}
        />
        <AuthorField
          className="mr-3 my-1"
          authorName={props.data.author}
          authorURL={props.data.author_url}
        />
        <DateField className="mr-3 my-1" timestamp={props.data.date} />
        <RatingField
          className="mr-3 my-1"
          ratings={props.data.ratings}
          rating={props.data.rating}
        />
        <CommentsField className="mr-3 my-1" comments={props.data.comments} />
      </Card.Header>
      <Link
        to={{
          pathname: "/recipe/" + encodeURIComponent(props.id),
          state: { query: props.query }
        }}
        style={{ color: "inherit", textDecoration: "none" }}
      >
        <Card.Body className="d-md-flex p-0 m-0">
          <div>
            <RecipeThumbnail
              className="d-none d-md-inline"
              src={"/images/" + props.data.image}
            />
          </div>
          <div>
            <Image
              className="d-md-none"
              src={"/images/" + props.data.image}
              style={{
                objectFit: "cover",
                width: "100%",
                height: "calc(var(width)/20)"
              }}
            />
          </div>
          <div className="px-md-4" style={{ overflow: "auto", width: "100%" }}>
            <NutrientChart
              recommended={props.recommended}
              nutrients={props.data.nutrients}
            />
          </div>
        </Card.Body>
      </Link>
      <Link
        to={{
          pathname: "/recipe/" + encodeURIComponent(props.id),
          state: { query: props.query }
        }}
        style={{ color: "inherit", textDecoration: "none" }}
      >
        <Card.Footer>
          <DescriptionField description={props.data.description} />
        </Card.Footer>
      </Link>
    </Card>
  );
};

export default SearchResult;
