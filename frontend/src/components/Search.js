import React from "react";
import { useLocation } from "react-router-dom";

import SearchResult from "./SearchIndex";
import SearchResults from "./SearchResults";

const Search = () => {
  const params = new URLSearchParams(useLocation().search);
  const query = params.get("query");
  if (query !== null) {
    return <SearchResults key={query} query={query} />;
  } else {
    return <SearchResult/>;
  }
};

export default Search;
