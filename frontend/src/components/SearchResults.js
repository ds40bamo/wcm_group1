import React, { useState, useEffect } from "react";
import SearchBar from "./SearchBar";
import SearchResult from "./SearchResult";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import InfiniteScroll from "react-infinite-scroller";

import { search, getRecommended } from "../common/api";

const SearchResults = props => {
  const [results, setResults] = useState([]);
  const [recommended, setRecommended] = useState(null);
  const [hasMore, setHasMore] = useState(true);
  const size = 10;

  useEffect(() => {
    getRecommended()
      .then(response => response.json())
      .then(data => setRecommended(data));
  }, []);

  const loadMore = page => {
    const lower = (page - 1) * size;
    search(props.query, lower, size)
      .then(response => response.json())
      .then(fetchedResults => {
        if (fetchedResults.length < size) {
          setHasMore(false);
        }
        setResults(
          results.concat(
            fetchedResults.map(result => {
              const data = result._source;
              data["id"] = result._id;
              return data;
            })
          )
        );
      });
  };

  const loader = (
    <Container key="spinner" className="d-flex justify-content-center">
      <Spinner animation="border" style={{ margin: "auto" }} />
    </Container>
  );

  if (recommended !== null) {
    return (
      <>
        <SearchBar query={props.query} />
        <div className="pt-4"></div>
        <Container>
          <InfiniteScroll
            pageStart={0}
            loadMore={loadMore}
            hasMore={hasMore}
            loader={loader}
          >
            {results.map((data, i) => (
              <SearchResult
                key={i}
                data={data}
                recommended={recommended}
                id={data.id}
                query={props.query}
              />
            ))}
          </InfiniteScroll>
        </Container>
      </>
    );
  } else {
    return <></>;
  }
};

export default SearchResults;
