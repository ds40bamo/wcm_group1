import React, { useState, useEffect } from "react";
import SearchBar from "./SearchBar";
import SearchResult from "./SearchResult";
import Container from "react-bootstrap/Container";

import { getSimilarityIds, getMultiple, getRecommended } from "../common/api";

const SimiarityResults = props => {
  const [results, setResults] = useState([]);
  const [recommended, setRecommended] = useState(null);
  let query = undefined;
  const location = props.location;
  if (location !== undefined) {
    const state = location.state;
    if (location !== undefined) {
      if (state !== undefined) {
        query = state.query;
      }
    }
  }
  useEffect(() => {
    getRecommended()
      .then(response => response.json())
      .then(data => setRecommended(data));
    getSimilarityIds(props.match.params.id)
      .then(response => response.json())
      .then(async ids => {
        let res = [];
        for (const request of getMultiple(ids)) {
          const recipe = await request.then(response => response.json());
          res = res.concat([recipe]);
          setResults(res);
        }
      });
  }, [props.match.params.id]);

  if (recommended !== null) {
    return (
      <>
        <SearchBar query={query} />
        <div className="pt-4"></div>
        <Container>
          {results.map((data, i) => (
            <SearchResult
              key={i}
              data={data}
              recommended={recommended}
              id={data.id}
              query={query}
            />
          ))}
        </Container>
      </>
    );
  } else {
    return <></>;
  }
};

export default SimiarityResults;
