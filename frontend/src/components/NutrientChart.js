import React from "react";
import { HorizontalBar } from "react-chartjs-2";

const getRatio = (nutrients, recommended, entrieName) => {
  const nutrientInfo      = nutrients[entrieName]
  const recommendedInfo   = recommended[entrieName]
  const nutrientAmount    = nutrientInfo["magnitude"]
  const nutrientUnit      = nutrientInfo["unit"]
  const recommendedAmount = recommendedInfo["magnitude"]
  const recommendedUnit   = recommendedInfo["unit"]

  if (nutrientUnit !== recommendedUnit) {
    console.log(nutrientUnit + " " + recommendedUnit)
  }

  return nutrientAmount/recommendedAmount*100
}

const NutrientChart = props => {
  const recommended = props.recommended
  const nutrients   = props.nutrients
  const portion     = 100  // in gramm

  const carbs = nutrients["Carbohydrate, by difference"]["magnitude"]
  const fat   = nutrients["Total lipid (fat)"]["magnitude"]
  const sugar = nutrients["Sugars, total including NLEA"]["magnitude"]

  const carbsRatio     = carbs/portion*100
  const fatRatio       = fat/portion*100
  const sugarRatio     = sugar/portion*100

  const energyRatio    = getRatio(nutrients, recommended, "Energy KCAL")
  const proteinRatio   = getRatio(nutrients, recommended, "Protein")
  const calciumRatio   = getRatio(nutrients, recommended, "Calcium, Ca")
  const ironRatio      = getRatio(nutrients, recommended, "Iron, Fe")
  const zincRatio      = getRatio(nutrients, recommended, "Zinc, Zn")
  const magnesiumRatio = getRatio(nutrients, recommended, "Magnesium, Mg")
  
  const nutrientLabels = ["Energy", "Protein", "Carbohydrate", "Fat", "Sugar", "Calcium", "Iron", "Zinc", "Magnesium"]
  const nutrientData = [energyRatio, proteinRatio, carbsRatio, fatRatio, sugarRatio, calciumRatio, ironRatio, zincRatio, magnesiumRatio]

  const backgroundColor = ["rgba(255,255,0,0.3)", "rgba(102,255,51,0.3)", "rgba(180,180,180,0.3)", "rgba(255,102,0,0.3)", "rgba(0,255,255,0.3)", "rgba(180,180,180,0.3)", "rgba(183,65,14,0.3)", "rgba(204,0,255,0.3)", "rgba(0,255,0,0.3)"]
  const borderColor = ["rgb(255,200,0)", "rgb(102,255,51)", "rgb(180,180,180)", "rgb(255,102,0)", "rgb(0,255,255)", "rgb(180,180,180)", "rgb(183,65,14)", "rgb(204,0,255)", "rgb(0,255,0)"]
              
  const data = {
    labels: nutrientLabels,
    datasets: [
      {
        data: nutrientData,
        borderWidth: 1,
        backgroundColor: backgroundColor,
        borderColor: borderColor,
        fill: false
      }
    ]
  };
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      enabled: false
    },
    scales: {
      xAxes: [
        {
          stacked: true,
          gridLines: {
            display:false
          },
          ticks: {
            min: 0,
            max: 100
          }
        }
      ],
      yAxes: [
        {
          stacked: true,
          gridLines: {
            display:false
          },
          ticks: {
            mirror: true,
            padding: -10,
            min: 0,
            max: 100
          }
        }
      ]
    },
    layout: {
      padding: {
        top: 3
      }
    }
  };
  return (
    <>
      <HorizontalBar
        data={data}
        options={options}
      />
    </>
  );
};

export default NutrientChart;
