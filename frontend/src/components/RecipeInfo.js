import React, { useEffect, useState } from "react";
import SearchBar from "./SearchBar";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";
import { get, getRecommended } from "../common/api";
import NutrientChart from "./NutrientChart";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

import {
  AuthorField,
  RatingField,
  CommentsField,
  MainCategoriesField,
  CategoriesField,
  DescriptionField,
  CuisineField,
  DateField,
  PublisherField,
  IngredientField,
  InstructionsField
} from "./InfoFields";

const RecipeInfo = props => {
  const [data, setData] = useState(null);
  const [recommended, setRecommended] = useState(null);
  let query = undefined;
  const location = props.location;
  if (location !== undefined) {
    const state = location.state;
    if (location !== undefined) {
      if (state !== undefined) {
        query = state.query;
      }
    }
  }
  useEffect(() => {
    const nutrientRequest = getRecommended();
    const dataRequest = get(props.match.params.id);
    nutrientRequest.then(response => {
      if (response.ok) {
        response.json().then(json => setRecommended(json));
      } else {
        setRecommended(undefined);
      }
    });
    dataRequest.then(response => {
      if (response.ok) {
        response.json().then(json => setData(json));
      } else {
        setData(undefined);
      }
    });
  }, [props.match.params.id]);
  if (data !== null && data !== undefined) {
    return (
      <>
        <SearchBar query={query} />
        <div className="pt-4"></div>
        {data !== null && (
          <Container>
            <div className="d-flex justify-content-between align-items-start">
              <h2 style={{ fontFamily: "monospace" }}>{data.title}</h2>
              <div className="d-flex">
                <Button className="m-1" variant="primary" href={data.url}>
                  go to source
                </Button>
                <Button
                  className="m-1"
                  as={Link}
                  to={{
                    pathname:
                      "/similarity/" +
                      encodeURIComponent(props.match.params.id),
                    state: { query: query }
                  }}
                  style={{ color: "white", textDecoration: "none" }}
                  variant="secondary"
                >
                  show similar
                </Button>
              </div>
            </div>
            <div className="d-flex flex-wrap">
              <PublisherField
                className="mr-3 m-1"
                publisher={data.publisher}
                url={data.url}
              />
              <AuthorField
                className="mr-3 m-1"
                authorName={data.author}
                authorURL={data.author_url}
              />
              <DateField className="mr-3 m-1" timestamp={data.date} />
              <MainCategoriesField
                className="mr-3 m-1"
                mainCategories={data.main_categories}
              />
              <CuisineField className="mr-3 m-1" cuisine={data.cuisine} />
            </div>
            <hr />
            <div className="d-md-flex align-items-start">
              <a className="mr-3" href={data.url}>
                <Image
                  className="p-2 border"
                  height="250px"
                  src={"/images/" + data.image}
                />
              </a>
              <div className="d-flex flex-column">
                <div className="d-sm-flex flex-row align-items-start">
                  <RatingField
                    className="m-2"
                    ratings={data.ratings}
                    rating={data.rating}
                  />
                  <CommentsField className="m-2" comments={data.comments} />
                </div>
                <CategoriesField className="m-2" categories={data.categories} />
                <Card className={"shadow-sm p-3 " + props.className}>
                  <DescriptionField
                    className="m-2"
                    description={data.description}
                  />
                </Card>
              </div>
            </div>
            <div className="my-4" style={{ height: "300px" }}>
              {recommended !== null && recommended !== undefined && (
                <NutrientChart
                  recommended={recommended}
                  nutrients={data.nutrients}
                />
              )}
            </div>
            <IngredientField ingredients={data.ingredients} />
            <InstructionsField instructions={data.steps} />
          </Container>
        )}
      </>
    );
  } else if (data === undefined) {
    return (
      <>
        <SearchBar />
        <h1>not found</h1>
      </>
    );
  } else {
    return <SearchBar />;
  }
};

export default RecipeInfo;
