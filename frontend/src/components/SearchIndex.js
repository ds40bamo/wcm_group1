import React, { useRef } from "react";
import FormControl from "react-bootstrap/FormControl";
import InputGroup from "react-bootstrap/InputGroup";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import { FaSearch } from "react-icons/fa";
import { GiSpoon } from "react-icons/gi";
import history from "../common/history";
import "../css/Logo.css";

const SearchResult = () => {
  const inputRef = useRef();

  const submit = () => {
    const inputValue = inputRef.current.value;
    if (inputValue !== "") {
      history.push("/search?query=" + encodeURIComponent(inputValue));
    }
  };

  const onKeyDown = e => {
    if (e.keyCode === 13) {
      submit();
    }
  };

  return (
    <>
      <Container
        className="d-flex justify-content-center"
        style={{ height: "80%", width: "60%" }}
      >
        <div className="flex-grow-1 align-self-center d-flex flex-column">
          <div className="d-flex flex-column">
            <GiSpoon
              id="logo"
              className="mr-5 align-self-center"
              size="120px"
            />
            <svg
              className="align-self-center"
              viewBox="0 0 200 50"
              style={{ width: "300px" }}
            >
              <path id="curve" d="M 0 10  Q 100 50 200 10" fill="transparent" />
              <text textLength="205">
                <textPath href="#curve">RECIPE SEARCH</textPath>
              </text>
            </svg>
          </div>
          <InputGroup className="shadow">
            <FormControl ref={inputRef} type="text" onKeyDown={onKeyDown} />
            <InputGroup.Append>
              <Button onClick={submit}>
                <FaSearch />
              </Button>
            </InputGroup.Append>
          </InputGroup>
        </div>
      </Container>
    </>
  );
};

export default SearchResult;
