import React from "react";
import ReactDom from "react-dom";
import { Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import history from "./common/history"
import App from "./App";

ReactDom.render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById("react-root")
);
