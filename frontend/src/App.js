import React from "react";
import { Route, Switch, Redirect } from "react-router";

import Search from "./components/Search";
import RecipeInfo from "./components/RecipeInfo";
import SimiarityResults from "./components/SimilarityResults";
import "./css/App.css"

const App = () => {
  return (
    <Switch>
      <Redirect from="/" to="/search" exact />
      <Route path="/search" component={Search} exact />
      <Route path="/recipe/:id" component={RecipeInfo} />
      <Route path="/similarity/:id" component={SimiarityResults} />
      <Route render={() => <h1>404</h1>} />
    </Switch>
  );
};

export default App;
