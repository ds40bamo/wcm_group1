const search = async (query, lower = 0, size = 10) => {
  const method = "GET";
  return fetch(
    "http://localhost/api/recipes?query=" +
      encodeURIComponent(query) +
      "&from=" +
      encodeURIComponent(lower) +
      "&size=" +
      encodeURIComponent(size),
    {
      method
    }
  );
};

const get = async id => {
  const method = "GET";
  return fetch("http://localhost/api/recipe/" + encodeURIComponent(id), {
    method
  });
};

const getRecommended = async () => {
  const method = "GET";
  return fetch("http://localhost/api/recommended", {
    method
  });
};

const getSimilarityIds = id => {
  const method = "GET";
  return fetch("http://localhost/api/similarity/" + encodeURIComponent(id), {
    method
  });
};

const getMultiple = ids => {
  return ids.map(id => get(id));
};

export { search, get, getRecommended, getSimilarityIds, getMultiple };
