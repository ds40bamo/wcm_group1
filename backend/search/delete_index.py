from os import getenv, path
from urllib import request

# load .env if avail
ENV_PATH = path.join(path.dirname(__file__), "../../.env")
try:
    from dotenv import load_dotenv
    load_dotenv(ENV_PATH)
except:
    pass

INDEX_URL = getenv("INDEX_URL") or "http://localhost:9200"
INDEX_NAME = getenv("INDEX_NAME") or "recipes"
INDEX_PATH = INDEX_URL + "/" + INDEX_NAME

def delete_index():
    req = request.Request(INDEX_PATH, method="DELETE")
    request.urlopen(req)

try:
    delete_index()
    print("success")
except:
    print("index diden't exist")
