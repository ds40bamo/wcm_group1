import glob
from os import getenv, path
import json
from elasticsearch import Elasticsearch
from flask import Flask, request
from flask_restful import Api, Resource
from recommendation import RECOMMENDED_AMOUNT

# load .env if avail
ENV_PATH = path.join(path.dirname(__file__), "../../.env")
try:
    from dotenv import load_dotenv
    load_dotenv(ENV_PATH)
except:
    pass

INDEX_URL = getenv("INDEX_URL") or "http://localhost:9200"
INDEX_NAME = getenv("INDEX_NAME") or "recipes"
RECIPE_DIR = getenv("RECIPE_DIR") or "/"
RECIPE_DIR = path.expanduser(RECIPE_DIR)

SITES_DIR = path.join(RECIPE_DIR, "sites")
FILE_EXPR = "*n.json"
GLOB = path.join(SITES_DIR, FILE_EXPR)

CREATE_BODY = {
    "mappings": {
        "properties": {
            "publisher":   {"type": "text"},
            "amount":      {"type": "keyword"},
            "author":      {"type": "text"},
            "categories":  {"type": "text"},
            "comments":    {"type": "integer"},
            "date":        {"type": "date"},
            "description": {"type": "text"},
            "image":       {"type": "keyword"},
            "ingredients": {
                "type": "nested",
                "enabled": False
            },
#             "ingredients": {"type": "nested",
#                             "properties": {
#                                 "ingredient": {"type": "text"},
#                                 "quantity":   {"type": "float"},
#                                 "unit":       {"type": "keyword"},
#                                 "whole_line": {"type": "text"}
#                             }
#                         },
            "preptime":    {"type": "keyword"},
            "ratings":     {"type": "integer"},
            "steps":       {"type": "text"},
            "title":       {"type": "text"},
            "totaltime":   {"type": "keyword"},
            "url":         {"type": "keyword"},
            "nutrition":   {"enabled": False}
#             "nutrients":   {
#                 "type": "nested",
#                 "properties": {
#                     "nutrient": {
#                         "type": "nested",
#                         "properties": {
#                             "magnitude": {"type": "float"},
#                             "unit":      {"type": "keyword"}
#                         }
#                     }
#                 }
#             }
        }
    }
}


FIELDS = [
    "publisher",
    "steps",
    "ingredients",
    "description",
    "title",
    "author",
    "categories",
    "category",
    "ingredients.ingredient",
    "ingredients.whole_line"
]

es = Elasticsearch([INDEX_URL], timeout=100)


def index_exists():
    return es.indices.exists(index=INDEX_NAME)


def create():
    es.indices.create(index=INDEX_NAME, body=CREATE_BODY)


def insert_from_file(file):
    publisher = path.basename(file).split("_")[0]
    with open(file) as f:
        body = []
        recipes = json.load(f)
        for recipe in recipes:
            img_path = recipe["images"][0]["path"]
            img_name = path.basename(img_path)
            recipe_id = path.splitext(img_name)[0]
            body.append(
                {"index": {
                    "_id": recipe_id
                }})
            recipe["image"] = img_name
            recipe["publisher"] = publisher
            body.append(recipe)
        es.bulk(index=INDEX_NAME, body=body)


if not index_exists():
    print("index didnt already exist")
    print("using: " + SITES_DIR)
    create()
    files = glob.glob(GLOB)
    if len(files) > 0:
        for file in files:
            print("found: " + file)
            insert_from_file(file)
    else:
        print("no files found")
else:
    print("index exists")
print()

app = Flask(__name__)
api = Api(app)

class SearchResource(Resource):
    def get(self):
        try:
            query = request.args["query"]
            body = {
                "query": {
                    "multi_match": {
                        "type": "most_fields",
                        "query": query,
                        "fields": FIELDS
                    }
                }
            }
            lower = request.args["from"]
            size = request.args["size"]
            body["from"] = lower
            body["size"] = size
            response = es.search(index=INDEX_NAME, body=body)
            return response["hits"]["hits"]
        except:
            return "", 400

class GetResource(Resource):
    def get(self, recipeid):
        try:
            response = es.get(index=INDEX_NAME, id=recipeid)
            source = response["_source"]
            source["id"] = recipeid
            return source
        except:
            return "", 400

class RecommendedResource(Resource):
    def get(self):
        try:
            return RECOMMENDED_AMOUNT, 200
        except:
            return "", 400

api.add_resource(SearchResource, "/recipes", endpoint="search")
api.add_resource(GetResource, "/recipe/<string:recipeid>", endpoint="get")
api.add_resource(RecommendedResource, "/recommended", endpoint="recommended")
app.run(host="0.0.0.0", port=5000, debug=True)
