RECOMMENDED_AMOUNT = {
    'Calcium, Ca': {'magnitude': 1000.0, 'unit': 'MG'},
    'Energy KCAL': {'magnitude': 2400.0, 'unit': 'KCAL'},
    'Energy KJ': {'magnitude': 10041.6, 'unit': 'kJ'},
    'Iodine, I': {'magnitude': 175.0, 'unit': 'UG'},
    'Iron, Fe': {'magnitude': 12.0, 'unit': 'MG'},
    'Magnesium, Mg': {'magnitude': 350.0, 'unit': 'MG'},
    'Protein': {'magnitude': 54.0, 'unit': 'G'},
    'Sugars, total including NLEA': {'magnitude': 97.81, 'unit': 'G'},
    'Total lipid (fat)': {'magnitude': 0.0, 'unit': 'G'},
    'Zinc, Zn': {'magnitude': 13.0, 'unit': 'MG'}
}
