import json
from glob import glob
from os import path

import numpy as np
from scipy.spatial import distance

DATAPATH = "/vector_data"
BINNAME = "sim.npy"
IDNAME = "id.npy"
BINPATH = path.join(DATAPATH, BINNAME)
IDPATH = path.join(DATAPATH, IDNAME)
SITESPATH = "/sites"

class Index():
    def __init__(self):
        try:
            self.ids, self.matrix = load_index()
        except:
            build_index()
            self.ids, self.matrix = load_index()

    def query(self, recipe):
        recipe_id, vector = getIdVector(recipe)
        def f(other):
            return distance.cosine(vector, other)
        res = sorted(enumerate(map(f, self.matrix)), key=lambda x: x[1])
        resIds, resScores = list(zip(*res))
        sortedIds = self.ids[list(resIds)]
        sortedIds = sortedIds[sortedIds != recipe_id]
        return sortedIds


def getAmount(nutrients, key):
    nutrientInfo = nutrients[key]
    nutrientAmount = nutrientInfo["magnitude"]

    return nutrientAmount


def getIdVector(recipe):
    nutrients = recipe["nutrients"]
    img_path = recipe["images"][0]["path"]
    img_name = path.basename(img_path)
    recipe_id = path.splitext(img_name)[0]

    energy    = getAmount(nutrients, "Energy KCAL")
    protein   = getAmount(nutrients, "Protein")
    carbs     = getAmount(nutrients, "Carbohydrate, by difference")
    fat       = getAmount(nutrients, "Total lipid (fat)")
    sugar     = getAmount(nutrients, "Sugars, total including NLEA")
    calcium   = getAmount(nutrients, "Calcium, Ca")
    iron      = getAmount(nutrients, "Iron, Fe")
    zinc      = getAmount(nutrients, "Zinc, Zn")
    magnesium = getAmount(nutrients, "Magnesium, Mg")

    nutrientData = [
        energy, protein, carbs, fat, sugar, calcium, iron, zinc, magnesium
    ]

    return recipe_id, np.array(nutrientData)


def build_index():
    results = []
    for file in glob(path.join(SITESPATH, "*n.json")):
        with open(file) as f:
            recipes = json.load(f)
            results.extend(map(getIdVector, recipes))
    ids, vectors = list(zip(*results))
    idsarray = np.array(ids)
    matrix = np.array(vectors)

    np.save(IDPATH, idsarray)
    np.save(BINPATH, matrix)


def load_index():
    return np.load(IDPATH), np.load(BINPATH)
