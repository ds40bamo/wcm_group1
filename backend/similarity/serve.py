import requests
from flask import Flask
from flask_restful import Api, Resource

from tools import Index

app = Flask(__name__)
api = Api(app)

index = Index()

class SimilarityResource(Resource):
    def get(self, id):
        try:
            recipe = requests.get("http://search:5000/recipe/" + id)
            ids = index.query(recipe.json())
            return list(ids[:10]), 200
        except Exception as e:
            print(e)
            return "", 404

api.add_resource(SimilarityResource, "/<string:id>", endpoint="similarity")


app.run(host="0.0.0.0", port=5000, debug=True)
