import re
from json import load
from pprint import pprint
from functools import reduce

from lark import Lark, Transformer
import pint
from unidecode import unidecode

strip_chars = ' !"#$%&\'*+,-./:;<=>?@\\^_`|~'
del_chars = ' !#$\'*,:;<=>?@\\^_`|~'

class MathRange:
    def __init__(self, lower, upper):
        # 1.0 to convert to float
        lower = 1.0 * lower
        upper = 1.0 * upper
        if lower > upper:
            raise ValueError("lower > upper")
        self.lower = lower
        self.upper = upper

    def __str__(self):
        return "[{}, {}]".format(self.lower, self.upper)

    def __add__(self, other):
        return MathRange(other+self.lower, other+self.upper)

    def __radd__(self, other):
        return MathRange(other+self.lower, other+self.upper)

    def __mul__(self, other):
        return MathRange(other*self.lower, other*self.upper)

    def __rmul__(self, other):
        return MathRange(other*self.lower, other*self.upper)

    def todict(self):
        if type(self.lower) is float:
            return {
                "range": {
                    "lower": self.lower,
                    "upper": self.upper
                }
            }
        else:
            return {
                "unit_range": {
                    "lower": self.lower.magnitude,
                    "upper": self.upper.magnitude,
                    "unit": str(self.lower.u)
                }
            }

number_value = {
    'a': 1,
    'an': 1,
    'zero': 0,
    'one': 1,
    'two': 2,
    'three': 3,
    'four': 4,
    'five': 5,
    'six': 6,
    'seven': 7,
    'eight': 8,
    'nine': 9,
    'ten': 10,
    'eleven': 11,
    'twelve': 12,
    'thirteen': 13,
    'fourteen': 14,
    'fifteen': 15,
    'sixteen': 16,
    'seventeen': 17,
    'eighteen': 18,
    'nineteen': 19,
    'twenty': 20,
    'thirty': 30,
    'forty': 40,
    'fifty': 50,
    'sixty': 60,
    'seventy': 70,
    'eighty': 80,
    'ninety': 90
}


grammar = """
    start: (amount_token|size_expr|other)*

// 1/2 cup to 2/3 cup / 1/5 g / 4 to 6 oz (16 to 24) 6-inch chicken
// 1/2 cup to 2/3 cup (5 g, 4 to 6 oz, 6-inch) chicken
// 1/2 cup to 2/3 cup 10 to 5 g / 4 to 6 oz (6-inch) chicken
    
    amount_token: _VAGUE* (amount|amount_times)

    !other.-100: WORD|"("|")"|","|"/"|"\\""|"'"|";"
    amount_times: times amount
    times.-100:         number "times"i?

    ?amount: quantity
           | amount_unit
           | amount_imprecise

    ?quantity.-100: range
             | number

    amount_imprecise: number (PACKAGE | imprecise_quantity PACKAGE?)

    ?amount_unit: unit_number
                | unit_range
                | unit_add

    unit_range:  (number|unit_number) "-"? _TO "-"? unit_number
    unit_add:    (number|unit_number|unit_range) _PLUS (unit_number|unit_range)
    unit_number: number "-"? unit

    size_expr: size ("-"? "thick")?

    ?size: size_unit_mul
         | size_unit_range

    size_unit_mul: ((number|size_unit_number) "-"? _MUL "-"?)+ size_unit_number
    size_unit_range: (number|size_unit) "-"? _TO "-"? size_unit
    size_unit_number: number "-"? size_unit

    range: number "-"? _TO "-"? number

    ?number.100: comp_frac
           | frac
           | real_number
           | WORD_NUMBER " "  -> word_number

    comp_frac: real_number (" "|"-") frac
    frac: real_number "/" real_number

    unit: (english_unit | metric_unit | fluid_ounce) /[^a-zA-Z]/

    PACKAGE: ("packages"i | "package"i)

    alcohol: number "-"? PROOF
    fat: percent_value "-"? FAT
    lean: percent_value "-" LEAN

    english_unit.100: CUP
        | GALLON
        | OUNCE
        | PINT
        | POUND
        | QUART
        | TABLESPOON
        | TEASPOON
        | CAN

    imprecise_quantity: DASH
        | HANDFUL
        | PINCH
        | TOUCH
        | STICK
        | LARGE
        | MEDIUM
        | SMALL
        | SCOOP
        | SPRIG
        | PACKET
        | BOTTLE
        | SPLASH

    metric_unit: GRAM
        | KILOGRAM
        | LITER
        | MILLIGRAM
        | MILLILITER

    CUP: "cups"i
        | "cup"i
        | "c."i
        | "c"i

    ?fluid_ounce: FLUID /\s+/ OUNCE

    FLUID: "fluid"i 
        | "fl."i 
        | "fl"i

    GALLON: "gallons"i
        | "gallon"i
        | "gal."i
        | "gal"i

    OUNCE: "ounces"i
        | "ounce"i
        | "oz."i
        | "oz"i

    PINT: "pints"i
        | "pint"i
        | "pt."i
        | "pt"i

    POUND: "pounds"i
        | "pound"i
        | "lbs."i
        | "lbs"i
        | "lb."i
        | "lb"i

    QUART: "quarts"i
        | "quart"i
        | "qts."i
        | "qts"i
        | "qt."i
        | "qt"i

    TABLESPOON: "tablespoons"i
        | "tablespoon"i
        | "tbsp."i
        | "tbsp"i
        | "tbs."i
        | "tbs"i
        | "T."i
        | "T"i

    TEASPOON: "teaspoons"i
        | "teaspoon"i
        | "tsp."i
        | "tsp"i
        | "t."i
        | "t"i

    GRAM: "grams"i
        | "gram"i
        | "gr."i
        | "gr"i
        | "g."i
        | "g"i

    KILOGRAM: "kilograms"i
        | "kilogram"i
        | "kg."i
        | "kg"i

    LITER: "liters"i
        | "liter"i
        | "l."i
        | "l"i

    MILLIGRAM: "milligrams"i
        | "milligram"i
        | "mg."i
        | "mg"i

    MILLILITER: "milliliters"i
        | "milliliter"i
        | "ml."i
        | "ml"i

    DASH: "dashes"i | "dash"i
    HANDFUL: "handfuls"i | "handful"i
    PINCH: "pinches"i | "pinch"i
    TOUCH: "touches"i | "touch"i
    STICK: "sticks"i | "stick"i
    CAN: "cans"i | "can"i
    LARGE: "large"i | "lg."i | "lg"i
    MEDIUM: "medium"i | "md."i | "md"i
    SMALL: "small"i | "sm."i | "sm"i
    SCOOP: "scoops"i | "scoop"i
    SPRIG: "sprigs"i | "sprig"i
    PACKET: "packets"i | "packet"i
    BOTTLE: "bottles"i | "bottle"i
    SPLASH: "splashes"i | "splash"i
    SPRAY: "spray"i | "sprays"i
    //    CLOVE: "cloves"i | "clove"i
    
    PROOF: "proof"
    FAT: "fat"
       | "dark"
       | "cacao"
    LEAN: "lean"


    WORD_NUMBER: "zero"i
        | "two"i
        | "twenty"i
        | "twelve"i
        | "three"i
        | "thirty"i
        | "thirteen"i
        | "ten"i
        | "sixty"i
        | "sixteen"i
        | "six"i
        | "seventy"i
        | "seventeen"i
        | "seven"i
        | "one"i
        | "ninety"i
        | "nineteen"i
        | "nine"i
        | "fourteen"i
        | "four"i
        | "forty"i
        | "five"i
        | "fifty"i
        | "fifteen"i
        | "eleven"i
        | "eighty"i
        | "eighteen"i
        | "eight"i
        | "an"i
        | "a"i


    _TO: "~"i
       | "-"i
       | "to"i

    _MUL: "*"i
        | "x"i
        | "by"i

    _PLUS: "plus"i
         | "+"i

    percent_value: number+ _PERCENT
    _PERCENT: "percent"i
            | "%"i

    size_unit: INCH

    INCH: "inches"i
        | "inch"i
        | "\\""i

    _VAGUE: "from"i
          | "about"i
          | "rounded"i
          | "whole"i
          | "scant"i

    _special_sep.20: "/"i
                   | _SEP
                   | _CONJ_SEP

    _CONJ_SEP: "&"i
             | "and/or"i
             | "and"i
             | "or"i

    _SEP: ","i
        | ";"i


    DIGIT: "0".."9"
    INT: DIGIT+
    DECIMAL: INT "." INT?
    real_number: DECIMAL  -> decimal
               | INT      -> int

    LETTER: "a".."z"
    WORD: LETTER("-"?LETTER)*

    %import common.WS
    %ignore WS
"""

punct_re = re.compile("[/():-]")

class RecipeTransformer(Transformer):
    def __init__(self):
        super(RecipeTransformer, self)
        self.ureg = pint.UnitRegistry()
    def start(self, item):
        result = []
        other = []
        for token in (filter(lambda x: x is not None, item)):
            try:
                other.append(token["other"])
            except:
                if other:
                    result.append({ "other": " ".join(other) })
                    other = []
                result.append(token)
        if other:
            result.append({ "other": " ".join(other) })

        result = filter(lambda x: "other" not in x or not punct_re.match(x["other"]), result)
        return list(result)
    def other(self, item):
        if item:
            return { "other" : str(item[0]) }
        else:
            return None
    def int(self, item):
        return int(item[0])
    def decimal(self, item):
        return float(item[0])
    def word_number(self, item):
        return number_value[item[0]]
    def comp_frac(self, item):
        return sum(map(float, item))
    def frac(self, item):
        numerator, denominator = item
        return float(numerator)/float(denominator)
    def amount_times(self, item):
        times, amount = item
        return float(times) * amount
    def times(self, item):
        return item[0]
    def total(self, item):
        return item[0]
    def lower(self, item):
        return item[0]
    def upper(self, item):
        return item[0]
    def range(self, item):
        lower, upper = map(float, item)
        if lower > upper:
            return lower + upper
        else:
            return MathRange(lower, upper)
    def amount_token(self, item):
        amount, = item
        try:
            return amount.todict()
        except:
            try:
                return {
                    "unit_total": {
                        "magnitude": amount.magnitude,
                        "unit": str(amount.u)
                    }
                }
            except:
                pass
        return { "total": amount }
    def fluid_ounce(self, item):
        return "floz"
    def english_unit(self, item):
        unit, = item
        return unit.type.lower()
    def metric_unit(self, item):
        unit, = item
        return unit.type.lower()
    def unit(self, item):
        unit, _ = item
        return unit
    def unit_range(self, item):
        lower, upper = item
        if type(lower) != type(upper):
            lower = lower * upper.units
        return MathRange(lower, upper)
    def unit_add(self, item):
        return item[0] + item[1]
    def unit_number(self, item):
        return item[0] * self.ureg(item[1])
    def size_expr(self, item):
        size, = item
        try:
            return size.todict()
        except:
            return {
                "size": {
                    "magnitude": size.magnitude,
                    "unit": str(size.u)
                }
            }
    def size_unit(self, item):
        name, = item
        return str(name)
    def size_unit_mul(self, item):
        def red(left, right):
            if type(left) != type(right):
                left = left * right.units
            return left * right
        return reduce(red, reversed(list(item)))
    def size_unit_number(self, item):
        return item[0] * self.ureg(item[1])
    def size_unit_range(self, item):
        lower, upper = item
        if type(lower) != type(upper):
            lower = lower * upper.units
        return MathRange(lower, upper)
    def quantity_token(self, item):
        quantity, = item
        return quantity
    def imprecise_quantity(self, item):
        return "IMPRECISE"
    def amount_imprecise(self, item):
        return " ".join(map(str, item))


left_parent_stip = re.compile("\(([{}])+".format(strip_chars))
right_parent_stip = re.compile("([{}])+\)".format(strip_chars))
too_long = re.compile("\(.*\)")
parent_translator = str.maketrans("[{}]", "(())")
char_del_translator = str.maketrans(del_chars, " "*len(del_chars))

def parenthese_del(arg):
    ret = []
    inner = []
    depth = 0
    for c in arg:
        if c == "(":
            depth += 1
        if depth == 1:
            inner.append(c)
        if depth == 0:
            ret.append(c)
        if c == ")":
            if depth == 0:
                ret = []
            else:
                depth -= 1
                if depth == 0:
                    ret.extend(inner)
                    inner = []

    return "".join(ret)


def quotation_del(arg):
    ret = []
    last = "a"
    for c in arg:
        if not c == '"' or last.isnumeric():
            ret.append(c)
            last = c

    return "".join(ret)


def normalize(ingredient):
    ingredient = unidecode(ingredient)
    ingredient = ingredient.translate(parent_translator)
    ingredient = ingredient.translate(char_del_translator)
    ingredient = ingredient.replace("½", " 1/2")
    ingredient = ingredient.replace("¼", " 1/4")
    ingredient = ingredient.replace("¾", " 3/4")
    ingredient = ingredient.replace("⅓", " 1/3")
    ingredient = ingredient.replace("⅔", " 2/3")
    ingredient = ingredient.replace("⅛", " 1/8")
    ingredient = ingredient.replace("⅐", " 1/7")
    ingredient = ingredient.replace("⅑", " 1/9")
    ingredient = ingredient.replace("⅒", " 1/10")
    ingredient = ingredient.replace("⅕", " 1/5")
    ingredient = ingredient.replace("⅖", " 2/5")
    ingredient = ingredient.replace("⅗", " 3/5")
    ingredient = ingredient.replace("⅘", " 4/5")
    ingredient = ingredient.replace("⅙", " 1/6")
    ingredient = ingredient.replace("⅚", " 5/6")
    ingredient = ingredient.replace("⅜", " 3/8")
    ingredient = ingredient.replace("⅝", " 5/8")
    ingredient = ingredient.replace("⅞", " 7/8")
    ingredient = ingredient.replace("⁄", "/")
    ingredient = ingredient.replace("'", "")
    ingredient = ingredient.replace("[", "(")
    ingredient = "".join([c for c in ingredient if ord(c) < 128])
    ingredient = ingredient.strip(strip_chars)
    ingredient = left_parent_stip.sub("(", ingredient)
    ingredient = right_parent_stip.sub(")", ingredient)
    ingredient = ingredient.lower()
#     ingredient = parenthese_del(ingredient)
#     ingredient = quotation_del(ingredient)
    return ingredient


parser = Lark(grammar)
transformer = RecipeTransformer()

def parse(line):
    line = normalize(line)
    try:
        res = transformer.transform(parser.parse(line))
#         print()
#         print(line)
#         pprint(res)
        return res
    except Exception:
#         print(line)
#         print("-------------------------------")
        return []

def main():
    FILES = [
        "chowhound_2020-01-24T10-31-56.json",
    ]
    for i in range(len(FILES)):
        with open(FILES[i]) as f:
            l = load(f)
            i = 0
            for e in l:
#                 print(i)
                i += 1
                ingredients = e["ingredients"]
                url = e["url"]
                for ingredient in ingredients:
                    parse(ingredient)


if __name__ == "__main__":
    main()
