from nltk.corpus import wordnet


class Node():
    def __init__(self, synset):
        self.synset = synset
        synset_split = synset.name().split('.')
        self.lemma = synset_split[0]
        self.pos = synset_split[1]
        self.number = synset_split[2]

    def __eq__(self, other):
        type_match = self.__class__ == other.__class__
        if type_match:
            return (self.lemma == other.lemma and self.pos == other.pos and self.number == other.number)
        return False

    def __lt__(self, other):
        type_match = self.__class__ == other.__class__
        if type_match:
            return self.lemma < other.lemma
        return False

    def __hash__(self):
        return hash((self.lemma, self.pos, self.number))


class SynsetTree():
    def __init__(self):
        self.synsets = [
            wordnet.synset('fruit.n.01'),
            wordnet.synset('seed.n.01'),
            wordnet.synset('nut.n.01'),
            wordnet.synset('nutriment.n.01'),
            wordnet.synset('nutrient.n.02'),
            wordnet.synset('foodstuff.n.02'),
            wordnet.synset('plant.n.02'),
            wordnet.synset('edible_fat.n.01'),
            wordnet.synset('carbohydrate.n.01'),
            wordnet.synset('alcohol.n.01'),
            wordnet.synset('meat.n.01'),
            wordnet.synset('salt.n.01'),
            wordnet.synset('press_out.v.03'),
            wordnet.synset('mixture.n.01'),
            wordnet.synset('powder.n.01'),
            wordnet.synset('seafood.n.01'),
            wordnet.synset('fish.n.01'),
            wordnet.synset('unsalted.s.01'),
            wordnet.synset('fungus.n.01'),
            wordnet.synset('plant_part.n.01'),
            wordnet.synset('pancake.n.01'),
            wordnet.synset('whip.v.03'),

            wordnet.synset('chocolate.n.02'),
            wordnet.synset('pasta.n.02'),
        ]
        self.nodes = []
        for synset in self.synsets:
            self.nodes += list(set(self._create_nodes(synset)))
        self.nodes = sorted(self.nodes, key=lambda x: x.lemma)
        self.num_nodes = len(self.nodes)

        self._connect_nodes()

    def _accumulate_synsets(self, synset_root):
        subtree_synsets = set([synset_root])
        for hyponym in synset_root.hyponyms():
            subtree_synsets.add(hyponym)
            subtree_synsets.update(self._accumulate_synsets(hyponym))
        return subtree_synsets

    def _create_nodes(self, synset_root):
        nodes = []
        for synset in self._accumulate_synsets(synset_root):
            nodes.append(
                Node(synset)
            )
        return nodes

    def _connect_nodes(self):
        for node in self.nodes:
            node.hypernyms = []
            for hypernym in node.synset.hypernyms():
                possible_parent = Node(hypernym)
                parent = self.search_by_node(possible_parent)
                if parent:
                    node.hypernyms.append(parent)
            
            node.hyponyms = []
            for hyponym in node.synset.hyponyms():
                possible_child = Node(hyponym)
                child = self.search_by_node(possible_child)
                if child:
                    node.hyponyms.append(child)

    def _binary_search(self, query, field, R, L=0):
        if L > R:
            return None
        m = (L + R) // 2
        element = field[m]

        if element.lemma < query:
            return self._binary_search(query, field, R=R, L=m+1)
        elif element.lemma > query:
            return self._binary_search(query, field, R=m-1, L=L)
        else:
            return element

    def search_by_node(self, node):
        """
        Wrapper for _binary_search for searching by a given reference Node
        
        Parameters:
            node (Node): Reference node to search for in Trees nodelist
        Returns:
            node (Node): Binary search result
        """
        return self._binary_search(node.lemma, self.nodes, self.num_nodes-1)
    
    def search_by_lemma(self, lemma):
        """
        Wrapper for _binary_search for searching by a given reference Node
        
        Parameters:
            lemma (str): Reference string to search for in Trees nodelist
        Returns:
            node (Node): Binary search result
        """
        return self._binary_search(lemma, self.nodes, self.num_nodes-1)

    def neighbors(self, node):
        """
        Gives a list of neighbors of the given node in the tree structure
        If node is given by string the the corresponding node is retrieved first
        
        Parameters:
            node (Node/str): Reference Node (or lemma) the neighbors shall be retrieved for
        Returns:
            neighbors (list): Distinct neighbors
        """
        if type(node) == str:
            node = self.search_by_lemma(node)
        elif type(node) == Node:
            node = self.search_by_node(node)
        else:
            return None

        neighbors = set()
        for hypernym in node.hypernyms:
            neighbors.update(set(hypernym.hyponyms))
        return list(neighbors)

    def get_more_specific(self, lemma):
        """
        Gives hyponyms of a given lemma if there are any. Else it gives an empty list.

        Parameters:
            lemma (str): Parent for the hypernyms
        Returns:
            hyponyms (list): List of hyponyms if any, else []
        """
        node = self.search_by_lemma(lemma)
        if node:
            return [hyponym.lemma for hyponym in node.hyponyms]
        return []

    def get_less_specific(self, lemma):
        """
        Gives hypernym(s) of a given lemma if there are any. Else it gives an empty list.

        Parameters:
            lemma (str): Hyponym of the result
        Returns:
            hypernyms (list): List of hypernyms if any, else []
        """
        node = self.search_by_lemma(lemma)
        if node:
            return [hypernym.lemma for hypernym in node.hypernyms]
        return []
