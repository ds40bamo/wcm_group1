from json import load

import pandas as pd
from gensim import corpora, models, similarities
from nltk import WordNetLemmatizer, pos_tag, word_tokenize
from nltk.corpus import wordnet
from unidecode import unidecode

from synset_tree import SynsetTree
from tree2 import Tree

# nltk.download('wordnet')
# nltk.download('punkt')



entitys = {}

foodsynsets = [
    wordnet.synset('fruit.n.01'),
    wordnet.synset('seed.n.01'),
    wordnet.synset('nut.n.01'),
    wordnet.synset('nutriment.n.01'),
    wordnet.synset('nutrient.n.02'),
    wordnet.synset('foodstuff.n.02'),
    wordnet.synset('plant.n.02'),
    wordnet.synset('edible_fat.n.01'),
    wordnet.synset('carbohydrate.n.01'),
    wordnet.synset('alcohol.n.01'),
    wordnet.synset('meat.n.01'),
    wordnet.synset('salt.n.01'),
    wordnet.synset('press_out.v.03'),
    wordnet.synset('mixture.n.01'),
    wordnet.synset('powder.n.01'),
    wordnet.synset('seafood.n.01'),
    wordnet.synset('fish.n.01'),
    wordnet.synset('unsalted.s.01'),
    wordnet.synset('fungus.n.01'),
    wordnet.synset('plant_part.n.01'),
    wordnet.synset('pancake.n.01'),
    wordnet.synset('whip.v.03'),

    wordnet.synset('chocolate.n.02'),
    wordnet.synset('pasta.n.02'),
]

lemmatizer = WordNetLemmatizer()
syn_tree = SynsetTree()

def quotation_del(arg):
    ret = []
    last = "a"
    for c in arg:
        if not c == '"' or last.isnumeric():
            ret.append(c)
            last = c

    return "".join(ret)


def parenthese_del(arg):
    ret = []
    depth = 0
    for c in arg:
        if c == "(":
            depth += 1
        if depth == 0:
            ret.append(c)
        if c == ")":
            if depth == 0:
                ret = []
            else:
                depth -= 1

    return "".join(ret)


def categorize(token):
    lemmas = wordnet.synsets(token)
    if lemmas:
        for lemma in lemmas:
            for foodsynset in foodsynsets:
                if foodsynset in lemma.lowest_common_hypernyms(foodsynset):
                    entitys[token] = True
                    return True
    return False


# replace synonymical phrases so the query matches the database better
def replace_synonyms(query):
    replace_dict = {
        "without salt": "unsalted",
    }
    for syn in replace_dict:
        if syn in query:
            query = query.replace(syn, replace_dict[syn])
    return query


stopwords = ["a", "cups", "cup", "as", "cut", 'nfs']
# words that are not actually stopwords but words that shouldnt be classified as food words
not_food = ["clove", "cloves", "simple", "stalk", "ears", "kosher", "cake"]
stopwords += not_food

fooddata = pd.read_csv('fooddata/food.csv')[['fdc_id', 'description']]
fooddata.set_index('fdc_id', inplace=True)


def clean_data(line):
    return [lemmatizer.lemmatize(block) for block in
            replace_synonyms(line.replace(',', '').lower()).split()]

## B TREE TRY ##
def create_tree():
    db_t = Tree()

    text_corpus = []
    for line in fooddata['description'].tolist():
        line = clean_data(line)
        text_corpus.append(line)
    d = corpora.Dictionary(text_corpus)

    corpus = [d.doc2idx(document) for document in text_corpus]
    doc_ids = list(fooddata.index)
    db_t.train(corpus, doc_ids)
    return db_t, d

db_tree, dictionary = create_tree()
## LSI TRY ##
# def create_lsi(dims):
#     lemmatizer = WordNetLemmatizer()
#     text_corpus = [[lemmatizer.lemmatize(block) for block in line.replace(',', '').lower().split()] for line in fooddata['description'].tolist()]
#     dictionary = corpora.Dictionary(text_corpus)
#     corpus = [dictionary.doc2bow(document) for document in text_corpus]
#     tfidf = models.TfidfModel(corpus)
#     corpus_tfidf = tfidf[corpus]

#     lsi = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=dims)
#     corpus_lsi = lsi[corpus_tfidf]

#     index = similarities.MatrixSimilarity(corpus_lsi)

#     return dictionary, tfidf, lsi, index

# def match_ingredients(candidate, dictionary, tfidf, lsi, index):
#     lemmatizer = WordNetLemmatizer()
#     query = [lemmatizer.lemmatize(word) for word in candidate.split()]
#     query_vec = dictionary.doc2bow(query)
#     query_vec_lsi = lsi[tfidf[query_vec]]

#     sims = index[query_vec_lsi]
#     sims = sorted(list(enumerate(sims)), key=lambda x: x[1], reverse=True)
#     results = list(map(lambda x: fooddata.iloc[x[0]]['description'],
#                    sorted(sims[:500], key=lambda x: x[1]/len(fooddata.iloc[x[0]]['description'].split()), reverse=True)))[:3]
#     return results


# sorts already found possible matches by the amount of nouns not appearing in the query but in the
# possible match (ascending).
# Tries to find a tightest fit in the possible solutionset
def sort_by_num_unnecessary_nouns(query, possible_documents):
    groupers = ['Alcoholic beverage, ', 'Spices, ', 'whole ', 'Candies, ', 'Snacks, ']
    def count_nn(tagged_phrase):
        nns = 0
        for _, tag in tagged_phrase:
            if 'NN' in tag:
                nns += 1
        return nns
    def remove_except_nn(tagged_phrase):
        ret = set()
        for word, tag in tagged_phrase:
            if 'NN' in tag:
                ret.add((word, tag))
        return ret
    tagged_query = set(pos_tag(query))
    unnecessary_nn = []
    for doc in possible_documents:
        # translate document id (from possible_documents) to actual line and clean it
        doc = fooddata.loc[doc, 'description']
        for s in groupers:
            doc = doc.replace(s, '')
        tagged_doc = set(pos_tag(doc))
        # calculate number of not matching and therefore unnecessary NNs
        unnecessary_nn.append(count_nn(tagged_doc) -
                              len(remove_except_nn(tagged_doc).intersection(tagged_query)))
    try:
        return list(zip(*sorted(list(zip(possible_documents, unnecessary_nn)), key=lambda x: x[1])[:10]))[0]
#         return sorted(list(zip(possible_documents, unnecessary_nn)), key=lambda x: x[1])
    except IndexError:
        return []


# sorts already found possible matches by the number of words existing both in query and possible match
def sort_by_num_matching_words(query, possible_documents):
    query_set = set(query)
    matching_words = []
    for doc in possible_documents:
        doc_set = set(clean_data(fooddata.loc[doc]['description']))
        matching_words.append(len(query_set.intersection(doc_set)))
    try:
        return list(zip(*sorted(list(zip(possible_documents, matching_words)), key=lambda x: x[1], reverse=True)))[0]
    except IndexError:
        return []


# Idea here is that 
#   1) Nouns in a query are in front of a dataset row and therefore should be searched for first
#   2) n-grams of nouns are usually of the form "specification specification2 ... actual_food" and therefore the second word should be searched for first
def reorder_query(query):
    tagged_query = pos_tag(query)
    def reorder_noun_n_grams(tagged_query):
        new_query = []
        nn_bloc = []
        nn_bloc_started = False
        for word, pos in tagged_query:
            if pos != 'NN':
                if nn_bloc_started:
                    nn_bloc_started = False
                    new_query += nn_bloc[::-1]
                    nn_bloc = []
                new_query.append((word, pos))
            else:
                nn_bloc.append((word, pos))
                nn_bloc_started = True
        if nn_bloc_started:
            new_query += nn_bloc[::-1]

        return new_query

    def nouns_to_front(tagged_query):
        new_query = sorted(tagged_query, key=lambda x: 0 if x[1] == 'NN' else 1)
        return new_query

    return [word[0] for word in nouns_to_front(reorder_noun_n_grams(tagged_query))]


def parse_food(line):
    sent = []
    line = unidecode(line)
    line = parenthese_del(line)
    line = replace_synonyms(line)
    tokens = [i.lower() for i in word_tokenize(line) if i not in stopwords]
    # attributes for accumulating words as one ingredient
    found_food = False
    candidate = []
    separators = [',', 'or', 'and']
    for token in tokens:
        if found_food and (token in separators):
            break
        if token in entitys:
            isfood = entitys[token]
            found_food = isfood
        else:
            isfood = categorize(token)
            entitys[token] = isfood
            found_food = isfood

        if isfood:
            token_l = lemmatizer.lemmatize(token)
            if token_l in dictionary.token2id.keys():
                candidate.append(token_l)

            sent.append("\033[01;31m{}\033[00m".format(token))
        else:
            sent.append(token)

#     print(" ".join(sent))
    candidate = dictionary.doc2idx(reorder_query(candidate))

    return candidate


def find_match(candidate):
    matches = db_tree.classify(candidate[:])
    candidate = [dictionary[idx] for idx in candidate]
    if not matches:
        hyponyms = syn_tree.get_more_specific('_'.join(list(set(candidate))))
        if hyponyms:
            # Here we have to decide which hyponym we take
            i = 0
            while not matches and i < len(hyponyms):
                candidate = hyponyms[i].split('_')
                cand = dictionary.doc2idx(candidate)
                matches = db_tree.classify(cand)
                i += 1
    matches = sort_by_num_unnecessary_nouns(candidate, matches)
    matches = sort_by_num_matching_words(candidate, matches)
#     for match_index, match in enumerate(matches[:1]):
#         print('\tPossible match #{}: {}'.format(match_index, fooddata.loc[match]['description']))
    try:
        return candidate, matches[0]
    except IndexError:
        return candidate, None


def match(query):
    candidate = parse_food(query)
    candidate, match = find_match(candidate)
    return candidate, match


def main():
    with open("chowhound_2020-01-24T10-31-56.json") as f:
        data = load(f)

    for recipe in data:
        ingredients = recipe["ingredients"]
        for ingredient in ingredients:
            candidate = parse_food(ingredient)
            find_match(candidate)

if __name__ == "__main__":
    main()
    # create_tree()
