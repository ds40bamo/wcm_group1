class Node():
    def __init__(self, node_id, parent_node, document_id, token_window):
        self.node_id = node_id          # probably unnecessary
        self.token_window = token_window
        self.isleaf = True
        self.part_of_document = [document_id]
        self.parent = parent_node
        self.children = []

    def update(self, child_documents):
        self.part_of_document = list(set(self.part_of_document).union(set(child_documents)))
        if self.node_id != -1:
            self.parent.update(child_documents)

    def add_child(self, child_node):
        self.children.append(child_node)
        self.isleaf = False
#         for document_id in child_node.part_of_document:
        self.update(child_node.part_of_document)


class Tree():
    def __init__(self):
        self.root = Node(-1, None, -1, [-1])
        self.max_node_id = 0

    def _search_subtree(self, n_gram, node):
        for child in node.children:
            if n_gram == child.token_window:
                return child
        return False

    def _classify_search_subtree(self, token, node):
        found_nodes = []
        for child in node.children:
            if token in child.token_window:
                found_nodes.append(child)
        return found_nodes

    def train(self, x, y):
        assert len(x) == len(y), 'Number of training examples has to be equal to number of labels'
        for index in range(len(x)):
            example = x[index]
            label = y[index]

            current_node = self.root
            n = 1
            i = 0
            n_grams = []

            if len(example[i:]) < n:
                n_grams.append(example)
            while len(example[i:]) >= n:
                n_gram = example[i:i+n]
                n_grams.append(n_gram)
                i += 1
            for n_gram in n_grams:
                next_node = self._search_subtree(n_gram, current_node)
                if not next_node:
                    self.max_node_id += 1
                    next_node = Node(self.max_node_id, current_node, label, n_gram)
                    current_node.add_child(next_node)
                current_node = next_node

    def classify(self, query):
        query_cache = query[:]
        current_nodes = [self.root]
        while query:
            query_before = query[:]
            for token in query:
                total_new_nodes = []
                for node in current_nodes:
                    next_nodes = self._classify_search_subtree(token, node)
                    total_new_nodes += next_nodes
                if total_new_nodes:
                    current_nodes = total_new_nodes
                    query.remove(token)
            if query == query_before:
                break
        if current_nodes == [self.root]:
            return []
        possible_documents = []
        for node in current_nodes:
            possible_documents += node.part_of_document

        return possible_documents
