#!/usr/bin/python

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer


stop_words = ["raw", "nfs", "ripe", "year", "round", "average"]
tokenizer = RegexpTokenizer(r"[a-zA-Z]+")
lemmatizer = WordNetLemmatizer()

trans_dict = {
    "no": {
        "salt": {
            "added": None
        }
    },
    "sun": {
        "dried": "dried"
    },
    "with": {
        "salt": {
            None: "salted",
            "added": "salted"
        }
    },
    "without": {
        "salt":{
            None: "unsalted",
            "added": "unsalted"
        }
    }
}

def tokenize(sent):
    tokens = tokenizer.tokenize(sent)
    tokens = [lemmatizer.lemmatize(token) for token in tokens]
    old_tokens = tokens
    tokens = []
    i = 0
    while i < len(old_tokens):
        try:
            j = 0
            cur_obj = trans_dict
            while True:
                cur_obj = cur_obj [old_tokens[i+j]]
                if type(cur_obj) is str:
                    tokens.append(cur_obj)
                    break
                if cur_obj is None:
                    break
                j += 1
            i += j
        except:
            tokens.append(old_tokens[i])
        i += 1

    return tokens


def swap(text):
    tokens = text.split(",")
    ingr = tokens[0]
    if ingr.lower() == "oil":
        return ",".join([ingr + tokens[1]] + tokens[2:])
    return text


def clean(foods):
#   foods = list(filter(lambda x: len(x.split(",")[0].strip().split(" ")) <= 2, foods))
    foods = list(map(swap, foods))

    return foods


def gen_dtm(text):
    vec = CountVectorizer(binary=True, stop_words=stop_words, tokenizer=tokenize, dtype=bool)
    X = vec.fit_transform(text)
    dtm = pd.DataFrame(
        X.toarray(),
        columns=vec.get_feature_names(),
        dtype=bool
    )
    analyzer = vec.build_analyzer()

    return dtm, analyzer


class FoodClassifier():
    def __init__(self, food_file="food.csv"):
        food_df = pd.read_csv(food_file, index_col="fdc_id")
        foods = food_df.description
#         foods = clean(foods)
        foods = [food+"," if "," not in food else food for food in foods.values]
        parts = list(zip(*map(lambda food: food.split(",", 1), foods)))
        ingredient, specification = parts
        self.ingr_dtm, self.analyzer = gen_dtm(ingredient)
        self.spec_dtm, _ = gen_dtm(specification)
        self.trans_dtm = pd.DataFrame(map(" ".join, map(self.analyzer, foods)), columns=["name"])

    def classify(self, food):
        tokens = self.analyzer(food)
        ingr_tokens = [token for token in tokens if token in self.ingr_dtm]
        for i in reversed(range(1, len(ingr_tokens)+1)):
            candidate_tokens = ingr_tokens[:i]
            ingr_dtm = self.ingr_dtm
            avail_words = ingr_dtm.columns.intersection(candidate_tokens)
            candidates = ingr_dtm[ingr_dtm[avail_words].all(axis=1)]
            ingredients = candidates.sum(axis=1).sort_values()
            ingredients = ingredients[ingredients == ingredients.min()]
            if len(ingredients) > 0:
                spec_tokens = set(tokens) - set(candidate_tokens)
                spec_dtm = self.spec_dtm.loc[ingredients.index]
                avail_words = spec_dtm.columns.intersection(spec_tokens)
                candidates = spec_dtm[avail_words]
                specifications = candidates.sum(axis=1)
                specifications = specifications[specifications == specifications.max()]
                best_spec = spec_dtm.loc[specifications.index]
                best_spec = best_spec.sum(axis=1).sort_values()
                best_spec.name = "number"
                translated = pd.merge(best_spec, self.trans_dtm, left_index=True, right_index=True)
                translated.set_index("name", inplace=True)
                translated = translated["number"].sort_values()
                return list(translated[translated == translated.min()].index)

    def get(self, words, index="ingr"):
        tokens = self.analyzer(words)
        if index == "spec":
            dtm = self.spec_dtm
        elif index == "ingr":
            dtm = self.ingr_dtm
        else:
            raise Exception()
        avail_words = dtm.columns.intersection(tokens)
        if avail_words.empty:
            return None
        candidates = dtm[dtm[avail_words].all(axis=1)].sum(axis=1)
        candidates.name = "#words"
        translated = pd.merge(candidates, self.trans_dtm, left_index=True, right_index=True)
        translated.set_index("name", inplace=True)
        translated = translated["#words"].sort_values()
        return translated
