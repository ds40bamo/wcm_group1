import os
import json
import pandas as pd
from pint import UnitRegistry, UndefinedUnitError, DimensionalityError
from nltk import WordNetLemmatizer

import amountparser
import matcher


path = os.path.dirname(os.path.abspath(__file__))

class InvalidUnitError(Exception):
    pass


relevant_nutrient_ids = {
    1008: 'Energy KCAL',
    1062: 'Energy KJ',
    1003: 'Protein',
    1004: 'Total lipid (fat)',
    1258: 'Fatty acids, total saturated',
    1257: 'Fatty acids, total trans',
    1005: 'Carbohydrate, by difference',
    2000: 'Sugars, total including NLEA',
    1087: 'Calcium, Ca',
    1089: 'Iron, Fe',
    1090: 'Magnesium, Mg',
    1095: 'Zinc, Zn',
    1100: 'Iodine, I',

}

nutrient = pd.read_csv(path + '/fooddata/nutrient.csv')[['id', 'name', 'unit_name']]
nutrient.rename(columns={'id': 'nutrient_id'}, inplace=True)
nutrient.set_index('nutrient_id', inplace=True)


class Nutrients():
    def __init__(self, values=None):
        self.values = {}
        for n in relevant_nutrient_ids:
            name = relevant_nutrient_ids[n]
            self.values[name] = {
                'magnitude': values[name] if values and name in values else 0,
                'unit': nutrient.loc[n, 'unit_name']
            }

    def __add__(self, other):
        ret = {}
        for n in self.values:
            ret[n] = self.values[n]['magnitude']
        for n in other.values:
            if n in ret:
                ret[n] += other.values[n]['magnitude']
            else:
                ret[n] = other.values[n]['magnitude']
        return Nutrients(ret)

    def __truediv__(self, div):
        ret = {}
        if div == 0:
            return self
        for n in self.values:
            ret[n] = self.values[n]['magnitude'] / div
        return Nutrients(ret)

    def __str__(self):
        return json.dumps(self.values, indent=2)


ureg = UnitRegistry()
ureg.define('fluid = fluid_ounce')
lemmatizer = WordNetLemmatizer()

food = pd.read_csv(path + '/fooddata/food.csv')
food.set_index('fdc_id', inplace=True)
food_nutrient = pd.read_csv(path + '/fooddata/food_nutrient.csv')[['id', 'fdc_id', 'nutrient_id', 'amount']]
nutrient_lookup = nutrient.merge(food_nutrient, on='nutrient_id')
food_portion = pd.read_csv(path + '/fooddata/food_portion.csv')[
    ['id', 'fdc_id', 'amount', 'modifier', 'gram_weight']
]

def food_weight(query, mag=None, un=None):
    candidate, match = matcher.match(query)

    piece_synonyms = ['piece', 'pieces', 'package', 'serving', 'medium', 'steak'] + candidate
    def parse_amount(a):
        special_units = ['clove', 'cloves', 'stalk', 'stalks']
        # Case 1: Everything parsed well (unit_total key exists)
        if not a:
            raise InvalidUnitError
        if 'unit_total' in a[0]:
            magnitude = a[0]['unit_total']['magnitude']
            unit = a[0]['unit_total']['unit']
        # Case 2: no valid unit found -> unit is the ingredient itself
        elif 'total' in a[0]:
            magnitude = float(str(a[0]['total']).split()[0])
            unit = ''
            for entry in a[1:]:
                if 'other' in entry:
                    for spec in special_units:
                        if spec in entry['other']:
                            unit = lemmatizer.lemmatize(spec)
                            break
        # TODO: size field from grammar not considered yet
        else:
            magnitude = 0
            unit = ''
        unit = unit.split() if unit else piece_synonyms
        return magnitude, unit

    if not (mag and un):
        amount_dict = amountparser.parse(query)
        magnitude, unit = parse_amount(amount_dict)
    else:
        magnitude = mag
        try:
            ureg(un)
            unit = un.split()
        except UndefinedUnitError:
            unit = piece_synonyms

    available_unit_lines = food_portion[food_portion['fdc_id'] == match]
    def get_unit_matches(unit_to_match):
        probe = list(map(
            lambda s: parse_amount(amountparser.parse(
                str(int(s[0])) + ' ' + s[1] + ' stuff'
                ))[1],
            available_unit_lines.loc[:, ['amount', 'modifier']].values
            ))
        probe = list(
            map(
                lambda s: bool(
                    set(s).intersection(set(unit_to_match))
                ),
                probe)
        )
        unit_matches = available_unit_lines[probe]
        if not unit_matches.empty:
            return unit_matches.iloc[0]
        return unit_matches

    gram_weight = 0
    amount = 1
    conversion_factor = 1

    try:
        is_weight = ureg(str(magnitude) + ' ' + unit[0]).dimensionality == ureg('g').dimensionality
    except UndefinedUnitError:
        is_weight = False
    if is_weight:
        gram_weight = ureg(str(magnitude) + ' ' + unit[0]).to(ureg('g')).m
    else:
        unit_matches = get_unit_matches(unit)
        if unit_matches.empty:
            for line_id in available_unit_lines.index:
                portion_unit = parse_amount(amountparser.parse(
                    str(int(available_unit_lines.loc[line_id, 'amount'])) + ' ' +
                    available_unit_lines.loc[line_id, 'modifier'] + ' stuff'))[1]
                try:
                    u = ureg(unit[0])
                except UndefinedUnitError:
                    # in case unit == piece_synonyms or unit == some_special_unit
                    break
                try:
                    line_u = ureg(portion_unit[0])
                    conversion_factor = u.to(line_u).m
                    unit_matches = available_unit_lines.loc[line_id]
                    break
                except UndefinedUnitError:
                    continue
                except DimensionalityError:
                    continue

        if not unit_matches.empty:
            gram_weight = unit_matches.loc['gram_weight'] * conversion_factor
            amount = unit_matches.loc['amount']
            gram_weight = gram_weight / amount * magnitude
#             print('food_portion id: ', unit_matches.loc['id'], unit_matches.loc['modifier'])
        else:
            raise InvalidUnitError
    ingredient_name = food.loc[match, 'description'] if match else ''
    return match, round(gram_weight, 4), ingredient_name, magnitude, unit[0]


def get_nutrients(fdc_id=None, weight=0):
    if not (fdc_id and weight):
        return Nutrients()
    all_values = nutrient_lookup[nutrient_lookup['fdc_id'] == fdc_id]
    all_values.set_index('nutrient_id', inplace=True)
    values = {}
    for n in relevant_nutrient_ids:
        try:
            name = relevant_nutrient_ids[n]
            values[name] = weight/100 * all_values.loc[n, 'amount']
        except KeyError:
            values[name] = 0
    return Nutrients(values)
