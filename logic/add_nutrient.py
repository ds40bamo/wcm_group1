#!/usr/bin/python3

import json
import glob
import os
from multiprocessing import Pool

from nutrient import food_weight, InvalidUnitError, Nutrients, get_nutrients


basedir = os.path.expanduser(os.environ.get("RECIPE_DIR") or '~/.cache/recipecrawlers/')
sites = os.path.join(basedir, 'sites')
FILES = [os.path.basename(f) for f in glob.glob(sites+'/*.json') if os.path.isfile(f) and not f.endswith('-n.json')]
print(FILES)


def process_line(ind, e, site):
    try:
        ingredients = e['ingredients']
        inval_unit_counter = 0
        total_nutrients = Nutrients()
        total_weight = 0
        new_ingredients = []
        for ingredient in ingredients:
            if site == 'thepioneerwoman':
                ingredient = ingredients[ingredient] + ' ' + ingredient
            try:
                if site in ['chowhound', 'epicurious', 'simplyrecipes', 'skinnytaste', 'thepioneerwoman']:
                    fdc_id, weight, ingredient_name, magnitude, unit = food_weight(ingredient)
                elif site in ['foodcom']:
                    fdc_id, weight, ingredient_name, magnitude, unit = food_weight(
                        ingredient['food'],
                        mag=float(ingredient['quantity'].split('-')[0]),
                        un=ingredient['unit']
                    )
                elif site in ['yummly']:
                    fdc_id, weight, ingredient_name, magnitude, unit = food_weight(
                        ingredient['ingredient'],
                        mag=ingredient['quantity'],
                        un=ingredient['unit']
                    )
                else:
                    break
                total_weight += weight
                total_nutrients += get_nutrients(fdc_id, weight)

                new_ingredient = {
                    'ingredient': ingredient_name,
                    'quantity': magnitude,
                    'unit': unit,
                    'whole_line': ingredient
                }
                new_ingredients.append(new_ingredient)
            except InvalidUnitError:
                new_ingredient = {
                    'ingredient': '',
                    'quantity': 0,
                    'unit': '',
                    'whole_line': ingredient
                }
                new_ingredients.append(new_ingredient)
                inval_unit_counter += 1

        total_nutrients /= (total_weight / 100)
        e['ingredients'] = new_ingredients
        e['nutrients'] = total_nutrients.values

        print(10* '#' + ' FINISHED RECIPE {} | {} UNKNOWN UNIT(S) '.format(ind, inval_unit_counter) + 10*'#')
        return e
    except Exception:
        return None

def main():
    with Pool(8) as pool:
        for file_name in FILES:
            site = file_name.split('_')[0]
            print(file_name)
            with open(os.path.join(sites, file_name)) as in_file:
                lines = [(i, line, site) for i, line in enumerate(json.load(in_file))]
                ln = list(filter(lambda x: x is not None, pool.starmap(process_line, lines)))
                output_file_name = file_name[:-5] + '-n.json'
                with open(os.path.join(sites, output_file_name), 'w+') as out_file:
                    json.dump(ln, out_file)


if __name__ == "__main__":
    main()
